
import logging

from opensearchpy import OpenSearch

import settings

log = logging.getLogger(__name__)

def get_os_client():
    log.info("Using OpenSearch at %s:%s" % (settings.ES_HOST, settings.ES_PORT))

    auth = (settings.ES_USER, settings.ES_PWD) # For testing only. Don't store credentials in code.
    #ca_certs_path = '/full/path/to/root-ca.pem' # Provide a CA bundle if you use intermediate CAs with your root CA.

    os_client = OpenSearch(
        hosts = [{'host': settings.ES_HOST, 'port': settings.ES_PORT}],
        http_compress = True, # enables gzip compression for request bodies
        http_auth = auth,
        # client_cert = client_cert_path,
        # client_key = client_key_path,
        use_ssl = settings.ES_USE_SSL,
        verify_certs = settings.ES_VERIFY_CERTS,
        ssl_assert_hostname = False,
        ssl_show_warn = False,
    )
    return os_client