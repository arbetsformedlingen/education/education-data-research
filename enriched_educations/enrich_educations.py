import logging
import jmespath
import itertools
import copy
import concurrent.futures
import requests
import sys
import time
from multiprocessing import Value
from opensearch_common import get_os_client
from education_opensearch.opensearch_store import OpensearchStore
from education_opensearch.repository import opensearch_client
from opensearchpy.helpers import scan

import settings

log = logging.getLogger(__name__)

counter = Value('i', 0)

ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
ENRICHER_RETRIES = 10


def get_query_all_educations():
    query = {
        "track_total_hits": True,
        # "size": 100,
        "query": {"match_all": {}}
    }
    return query

def load_educations():
    os_client = get_os_client()

    query = get_query_all_educations()

    print("Using alias: %s" % settings.EDUCATIONS_ALIAS)


    results = scan(os_client, query, index=settings.EDUCATIONS_ALIAS)

    for item in results:
        yield item['_source']


# def test_load_educations():
#     educations = load_educations()
#     # for education in educations:
#     #     print(education)
#     print(len([item for item in educations]))


def get_doc_headline_input(education):
    sep = ' | '
    headline_values = []

    # education_title = jmespath.search("education.title[*].content", education)
    education_title = jmespath.search("education.title[?lang=='swe'].content", education)
    if education_title:
        headline_values.extend(education_title)

    education_subjects = jmespath.search("education.subject", education)
    #print(education_subjects)
    if education_subjects:
        educations_subjects_items = [subject['name'] for subject in education_subjects if 'name' in subject]
        headline_values.extend(educations_subjects_items)

    events = jmespath.search("events", education)
    if events:
        for event in events:
            if 'extension' in event:
                if 'keywords' in event['extension']:
                    keywords_swe = jmespath.search("extension.keywords[?lang=='swe'].content", event)
                    if keywords_swe:
                        # Some keywords are numeric, like: 2030
                        keywords_swe = [str(keyword) for keyword in keywords_swe]
                        # print('keywords_swe: %s' % keywords_swe)
                        headline_values.extend(keywords_swe)


    doc_headline_input = sep.join(headline_values)

    # print("doc_headline_input: %s" % doc_headline_input)

    return doc_headline_input

def get_doc_description_input(education):
    education_description = jmespath.search("education.description[?lang=='swe'].content | [0]", education)
    if not education_description:
        education_description = ''

    # print("doc_description_input: %s" % education_description)

    return education_description


def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])



def get_enrich_result(batch_indata, jae_api_endpoint_url, timeout):
    print('Getting result from endpoint: %s' % jae_api_endpoint_url)

    headers = {'Content-Type': 'application/json', 'api-key': settings.JAE_API_KEY}

    for retry in range(ENRICHER_RETRIES):
        try:
            r = requests.post(url=jae_api_endpoint_url, headers=headers, json=batch_indata, timeout=timeout)
            r.raise_for_status()
        except Exception as e:
            print(f"get_enrich_result() retrying #{retry + 1} after error: {e}")
            time.sleep(0.5)
        else:
            return r.json()
    print(f"_get_enrich_result failed after: {ENRICHER_RETRIES} retries with error. Exit!")
    sys.exit(1)


def execute_calls(batch_indatas, jae_api_endpoint_url, parallelism):
    global counter
    enriched_output = {}
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the load operations and mark each future with its URL
        future_to_enrich_result = {executor.submit(get_enrich_result, batch_indata, jae_api_endpoint_url,
                                                   settings.JAE_API_TIMEOUT_SECONDS): batch_indata
                                   for batch_indata in batch_indatas}
        for future in concurrent.futures.as_completed(future_to_enrich_result):
            try:
                enriched_result = future.result()
                for resultrow in enriched_result:
                    enriched_output[resultrow[ENRICHER_PARAM_DOC_ID]] = resultrow
                    with counter.get_lock():
                        counter.value += 1
                        if counter.value % 1000 == 0:
                            print(f'enrichtextdocuments - Processed docs: {counter.value}')
            except Exception as e:
                print(e)
                raise

    return enriched_output

def enrich(ad_batches, batch_indata_config, endpoint_url, parallelism):
    batch_indatas = []
    for i, ad_batch in enumerate(ad_batches):
        ad_batch_indatas = [ad_indata for ad_indata in ad_batch]
        batch_indata = copy.deepcopy(batch_indata_config)
        batch_indata['documents_input'] = ad_batch_indatas
        batch_indatas.append(batch_indata)
    print('len(batch_indatas): %s' % len(batch_indatas))
    enrich_results_data = execute_calls(batch_indatas, endpoint_url, parallelism)
    # print_debug('enrich_results_data: %s' % enrich_results_data)

    return enrich_results_data

def get_empty_output_value():
    return {
        "enriched_candidates": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        }
    }


def format_unique_concept_labels(enriched_candidates):
    labels = [candidate['concept_label'].lower() for candidate in enriched_candidates]
    labels = list(set(labels))
    return sorted(labels)


def add_enriched_result(educations_to_update, enriched_results_data):
    for education_to_update in educations_to_update:
        if not 'text_enrichments_results' in education_to_update:
            education_to_update['text_enrichments_results'] = {}
        doc_id = str(education_to_update.get('id', ''))
        if doc_id in enriched_results_data:
            enriched_results = enriched_results_data[doc_id]
            enriched_results_candidates = jmespath.search('enriched_candidates', enriched_results)
            enriched_output = get_empty_output_value()
            enriched_output_candidates = enriched_output['enriched_candidates']
            for attr_name in enriched_results_candidates.keys():
                enriched_candidates_for_type = format_unique_concept_labels(jmespath.search(attr_name, enriched_results_candidates))
                enriched_output_candidates[attr_name] = enriched_candidates_for_type
        else:
            # Set non valid ads to empty enriched values.
            enriched_output = get_empty_output_value()

        education_to_update['text_enrichments_results'] = enriched_output


def enrich_educations():

    parallelism = settings.ENRICH_CALLS_PARALLELISM

    if parallelism <= 0:
        parallelism = 1

    educations = load_educations()

    educations = [education for education in educations]

    nr_of_items_per_batch = 100

    # Prepare input
    ads_input_data = []
    for education in educations:
        # print(education)
        doc_id = str(education.get('id', ''))
        doc_headline = get_doc_headline_input(education)
        doc_text = get_doc_description_input(education)

        input_doc_params = {
            ENRICHER_PARAM_DOC_ID: doc_id,
            ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
            ENRICHER_PARAM_DOC_TEXT: doc_text
        }

        ads_input_data.append(input_doc_params)

    ad_batches = grouper(nr_of_items_per_batch, ads_input_data)

    batch_indata_config = {
        "include_terms_info": True,
        "include_sentences": False,
        # "sort_by_prediction_score": "DESC"
        "sort_by_prediction_score": "NOT_SORTED"
    }

    enrich_results = enrich(ad_batches, batch_indata_config, settings.JAE_API_URL + '/enrichtextdocuments', parallelism)
    add_enriched_result(educations, enrich_results)

    save_enriched_educations_in_repository(educations)


def save_enriched_educations_in_repository(enriched_educations):
    os_store = OpensearchStore()

    index_name = os_store.start_new_save(settings.ENRICHED_EDUCATIONS_ALIAS)

    print('Will save enriched occupations in index_name: %s' % index_name)

    os_store.save_educations_to_repository(enriched_educations)

    if not opensearch_client.alias_exists(settings.ENRICHED_EDUCATIONS_ALIAS):
        opensearch_client.put_alias(index_name, settings.ENRICHED_EDUCATIONS_ALIAS)
    else:
        current_alias = opensearch_client.get_alias(settings.ENRICHED_EDUCATIONS_ALIAS)
        opensearch_client.update_alias(index_name, [key for key in current_alias], settings.ENRICHED_EDUCATIONS_ALIAS)


# import os
# import json
# currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'
#
# def load_json_file(filepath):
#     with open(filepath, 'r', encoding='utf-8') as file:
#         data = json.load(file)
#         return data
#
# def load_educations_from_file():
#     educations = load_json_file(currentdir + '../resources/susanavet3.json')
#
#
#     # Prepare input
#     ads_input_data = []
#     for education in educations:
#         # print(education)
#         doc_id = jmespath.search('id', education)
#         doc_headline = get_doc_headline_input(education)


# load_educations_from_file()

# test_load_educations()

# educations = load_educations()
# print(len([education for education in educations]))

enrich_educations()


# clean_enriched_values(testvalue)