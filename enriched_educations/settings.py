import os

ENRICH_CALLS_PARALLELISM = int(os.getenv('ENRICH_CALLS_PARALLELISM', '8'))
# JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-api.jobtechdev.se')
JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-test-api.jobtechdev.se')
JAE_API_TIMEOUT_SECONDS = int(os.getenv('JAE_API_TIMEOUT_SECONDS', '60'))
JAE_API_KEY = os.getenv('JAE_API_KEY', 'api key here')
# EDUCATIONS_INDEX = os.getenv("EDUCATIONS_INDEX", "educations-susanavetcache-20211022-14.56")
EDUCATIONS_ALIAS = os.getenv("EDUCATIONS_ALIAS", "educations-susanavet")

# ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211025-17.08")
# ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211108-13.53")
# ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211116-11.33")

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 19200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv('ES_USE_SSL', 'false').lower() == 'true'
ES_VERIFY_CERTS = os.getenv('ES_VERIFY_CERTS', 'false').lower() == 'true'

VERSION = '0.0.1'
# ES_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations-susanavet-enriched')

ENRICHED_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations-susanavet-enriched')
ENRICHED_OCCUPATIONS_ALIAS = os.getenv('ENRICHED_OCCUPATIONS_ALIAS', 'enriched-occupations')


education_mappings = {
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            }
        }
    }
}

