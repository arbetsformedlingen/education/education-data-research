import pandas as pd
from pandas import json_normalize

# pd.set_option('display.max_columns', None)
# pd.set_option('display.max_rows', None)

import ndjson
def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data



def print_counts_for_susa_data(filepath):
    educations_json = load_ndjson_file(filepath)

    events_all = []
    locations_all = []
    providers_all = []
    for education in educations_json:
        for event in education['events']:
            for location in event['locations']:
                location['identifier'] = education['id']
                locations_all.append(location)
            events_all.append(event)
        for provider in education['education_providers']:
            providers_all.append(provider)
    print(f'Educations: {len(educations_json)}')
    print(f'Events: {len(events_all)}')
    print(f'Providers: {len(providers_all)}')
    print(f'Locations: {len(locations_all)}')
    educations_df = json_normalize(educations_json)

    education_form_counts = educations_df['education.form.code'].value_counts()
    print(f'Utbildningsformer:\n{education_form_counts}')
    locations_df = json_normalize(locations_all)
    # municipalities_df = locations_df['municipalityCode']
    municipalities_not_null = locations_df[pd.notnull(locations_df['municipalityCode'])]
    municipalities_null = locations_df[pd.isnull(locations_df['municipalityCode'])]
    print(f'Not empty municipalities: {len(municipalities_not_null)}')
    print(f'Empty municipalities: {len(municipalities_null)}')
    nr_unique_mun = len(municipalities_not_null['municipalityCode'].unique())
    print(f'Nr unique municipalityCodes: {nr_unique_mun}')
    # unique_mun = municipalities_not_null['municipalityCode'].unique()
    # print(f'Unique municipalityCodes: {unique_mun}')


susafile = 'resources/educations_susanavet_2022-12-15.jsonl'
# susafile = 'resources/educations_susanavet_2022-12-19.jsonl'
# susafile = 'resources/educations_susanavet_2022-12-19_paginate_municipalites.jsonl'
# susafile = 'resources/educations_susanavet_2022-12-15_minified.jsonl'

print_counts_for_susa_data(susafile)