# Education data research
The purpose of this repo is to make data researches, for example comparisons between different datasources and datasets.

# Getting started

## Set up virtual environment for this repo:
  Python version 3.8 or higher is required.  

- Activate a virtual env (e.g. conda or virtualenv):
  - Using conda:
    1. Go to https://conda.io/projects/conda/en/latest/user-guide/install/index.html and install Anaconda if it's not already installed on local machine
    2. Run the command
       ```conda create -n education python=3.8```
       ...to create a conda environment with the name 'education'
    3. In IntelliJ/PyCharm: `File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Conda Environment` and choose Existing and the environment 'education'
    
    - Using virtualenv: 
    1. In IntelliJ/PyCharm: `File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment -(a Python 3.8 installation as base interpretator)`
    
## Set up conda kernel
python -m ipykernel install --user --name education --display-name "Python (education)"

## Install requirements
- Choose your python environment in terminal
- Make sure you are in the root of this repo and run:
```
    pip install -r requirements.txt
```
