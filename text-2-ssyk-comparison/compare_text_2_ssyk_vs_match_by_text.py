import logging
import ndjson
import json
import os
import csv
import jmespath
import requests
from collections import Counter

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep


def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data


def load_json_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def read_taxonomy_ssyk_objects():
    filepath = currentdir + 'resources/Niva_fyra_i_SSYK-strukturen.json'
    ssyk_objects = load_json_file(filepath)
    # print(ssyk_objects)
    ssyk_concepts = jmespath.search('data.concepts', ssyk_objects)

    ssyk_descriptions = {item['ssyk_code_2012']: item['preferred_label'] for item in ssyk_concepts}

    return ssyk_descriptions


def read_taxonomy_yrkesbenamning_objects():
    filepath = currentdir + 'resources/yrkesomraden_med_relationer_till_ssyk_niva_fyra_och_yrkesbenamningar.json'
    yrke_objects = load_json_file(filepath)
    # print(ssyk_objects)
    yrkesomraden = jmespath.search('data.concepts', yrke_objects)

    yrke_to_yrkesgrupp_dict = {}

    for yrkesomrade in yrkesomraden:
        yrkesgrupper = jmespath.search('narrower', yrkesomrade)
        for yrkesgrupp in yrkesgrupper:
            yrkesgrupp_id = jmespath.search('id', yrkesgrupp)
            ssyk_code_2012 = jmespath.search('ssyk_code_2012', yrkesgrupp)
            yrkesgrupp_preferred_label = jmespath.search('preferred_label', yrkesgrupp)

            yrkesbenamningar = jmespath.search('narrower', yrkesgrupp)
            for yrkesbenamning in yrkesbenamningar:
                yrkesbenamning_id = jmespath.search('id', yrkesbenamning)
                yrkesbenamning_preferred_label = jmespath.search('preferred_label', yrkesbenamning)
                yrke_to_yrkesgrupp_dict[yrkesbenamning_id] = {
                    "yrkesbenamning_id": yrkesbenamning_id,
                    "yrkesbenamning_preferred_label": yrkesbenamning_preferred_label,
                    "yrkesgrupp_id": yrkesgrupp_id,
                    "ssyk_code_2012": ssyk_code_2012,
                    "yrkesgrupp_preferred_label": yrkesgrupp_preferred_label
                }

    return yrke_to_yrkesgrupp_dict


def get_match_by_text_result(input_headline, input_text, limit):
    # endpoint_url = 'http://localhost:5000/occupations/match-by-text'
    # endpoint_url = 'https://education-api-test.jobtechdev.se/occupations/match-by-text'
    # endpoint_url = 'http://localhost:5000/occupations/match-by-concepts-text'
    endpoint_url = 'http://education-api-localhost.jobtechdev.se:5000/occupations/match-by-concepts-text'
    log.debug('Getting result from endpoint: %s' % endpoint_url)
    # 'http://localhost:5000/occupations/match-by-text?input_text=%20python%20java%20javascript&input_headline=systemutvecklare&limit=10&offset=0&include_metadata=false'

    headers = {'accept': 'application/json'}

    json_input = {'input_headline': input_headline, 'input_text': input_text, 'limit': limit}

    try:
        r = requests.post(url=endpoint_url, headers=headers, json=json_input, timeout=20)
        r.raise_for_status()
    except Exception as e:
        log.info(f"get_match_by_text_result, error: {e}")
    else:
        return r.json()


def create_input_terms(ad):
    enriched_candidates = jmespath.search('text_enrichments_results.enrichedbinary_result.enriched_candidates', ad)
    enriched_occupations = jmespath.search('occupations', enriched_candidates)
    enriched_occupations_terms = [term['concept_label'].lower() for term in enriched_occupations]
    enriched_competencies = jmespath.search('competencies', enriched_candidates)
    enriched_competencies_terms = [term['concept_label'].lower() for term in enriched_competencies]
    input_terms = ' | '.join(enriched_occupations_terms + enriched_competencies_terms)
    return input_terms


def write_diff_to_csv_file(filename, diffing_comparison_data):
    if not diffing_comparison_data:
        print(f'Could not write file since there was no data, filename: {filename}')
        return

    keys_for_column_names = [key for key in diffing_comparison_data[0].keys()]
    filepath_result_not_same = currentdir + filename
    with open(filepath_result_not_same, 'w', newline='', encoding='utf-8-sig') as output_file:
        dict_writer = csv.DictWriter(output_file, keys_for_column_names, dialect='excel', delimiter=';')
        dict_writer.writeheader()

        dict_writer.writerows(diffing_comparison_data)
    print(f'Wrote file with diffs to {filepath_result_not_same}')


def compare_only_arbetsformedlingen_se():
    ssyk_objects = read_taxonomy_ssyk_objects()
    # ads_filepath = currentdir + 'resources/text_2_ssyk_minified.out.jsonl'
    # ads_filepath = currentdir + 'resources/text_2_ssyk_minified2.out.jsonl'
    ads_filepath = currentdir + 'resources/text_2_ssyk.out.jsonl'
    ads = load_ndjson_file(ads_filepath)
    yrkesbenamning_objects = read_taxonomy_yrkesbenamning_objects()

    ads_comparison_datas = []
    for counter, ad in enumerate(ads):
        original_job_posting = jmespath.search('originalJobPosting', ad)

        scraper = jmespath.search('scraper', original_job_posting)

        if scraper != 'arbetsformedlingen.se':
            continue

        ad_yrkesbenamning_id = jmespath.search('relevantOccupation.occupationalCategory.codeValue',
                                               original_job_posting)
        try:
            yrkesbenamning_object = yrkesbenamning_objects[ad_yrkesbenamning_id]
            ad_ssyk = jmespath.search('ssyk_code_2012', yrkesbenamning_object)
            ad_ssyk_description = jmespath.search('yrkesgrupp_preferred_label', yrkesbenamning_object)
        except KeyError:
            log.error(f'Could not find yrkesbenämning id: {ad_yrkesbenamning_id}. Skipping ad')
            continue
        ad_title = jmespath.search('title', original_job_posting)
        ssyk_text2ssyk = jmespath.search('ssyk_lvl4', ad)
        try:
            ssyk_description_text2ssyk = ssyk_objects[str(ssyk_text2ssyk)]
        except KeyError:
            ssyk_description_text2ssyk = 'Not valid SSYK'

        input_terms = create_input_terms(ad)

        # print(input_terms)
        match_by_text_result = get_match_by_text_result(input_terms, ' ', 1)

        if match_by_text_result and len(match_by_text_result['related_occupations']) > 0:
            related_occupation = match_by_text_result['related_occupations'][0]
            ssyk_match_by_text = jmespath.search('occupation_group.ssyk', related_occupation)
            ssyk_description_match_by_text = jmespath.search('occupation_group.occupation_group_label',
                                                             related_occupation)
            occupation_label_match_by_text = jmespath.search('occupation_label', related_occupation)
        else:
            ssyk_match_by_text = ''
            ssyk_description_match_by_text = ''
            occupation_label_match_by_text = ''

        ad_id = jmespath.search('id', ad)
        comparison_data = {
            "ad_title": ad_title,
            "ad_ssyk": ad_ssyk,
            "ad_ssyk_description": ad_ssyk_description,
            "ssyk_text2ssyk": str(ssyk_text2ssyk),
            "ssyk_description_text2ssyk": ssyk_description_text2ssyk,
            "ssyk_match_by_text": ssyk_match_by_text,
            "ssyk_description_match_by_text": ssyk_description_match_by_text,
            'occupation_label_match_by_text': occupation_label_match_by_text,
            "ad_id": ad_id,
            'ad_url': jmespath.search('url', original_job_posting)
        }
        ads_comparison_datas.append(comparison_data)

        if counter % 10 == 0:
            print(f'compare - Processed ads: {counter}')
        # print(f'compare - Processed ads: {counter}')

    # print(json.dumps(ads_comparison_datas, indent=4))

    same_ad_ssyk_text2ssyk = 0
    same_ad_ssyk_match_by_text = 0
    total_size = len(ads_comparison_datas)

    diffing_comparison_data_text2ssyk = []
    matching_comparison_data_text2ssyk = []
    diffing_comparison_data_match_by_text = []
    matching_comparison_data_match_by_text = []
    for ads_comparison_data in ads_comparison_datas:
        if ads_comparison_data['ad_ssyk'] == ads_comparison_data['ssyk_text2ssyk']:
            same_ad_ssyk_text2ssyk += 1
            matching_comparison_data_text2ssyk.append(ads_comparison_data)
        else:
            diffing_comparison_data_text2ssyk.append(ads_comparison_data)
        if ads_comparison_data['ad_ssyk'] == ads_comparison_data['ssyk_match_by_text']:
            same_ad_ssyk_match_by_text += 1
            matching_comparison_data_match_by_text.append(ads_comparison_data)
        else:
            diffing_comparison_data_match_by_text.append(ads_comparison_data)

    print(f'same_ad_ssyk_text2ssyk: {same_ad_ssyk_text2ssyk}, '
          f'same_ad_ssyk_match_by_text: {same_ad_ssyk_match_by_text}, '
          f'total_size: {total_size}')

    write_diff_to_csv_file('arbetsformedlingense_vs_text2ssyk_not_same_ssyk.csv', diffing_comparison_data_text2ssyk)
    write_diff_to_csv_file('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', diffing_comparison_data_match_by_text)
    write_diff_to_csv_file('arbetsformedlingense_vs_text2ssyk_same_ssyk.csv', matching_comparison_data_text2ssyk)
    write_diff_to_csv_file('arbetsformedlingense_vs_match_by_text_same_ssyk.csv', matching_comparison_data_match_by_text)


def compare():
    ssyk_objects = read_taxonomy_ssyk_objects()
    # ads_filepath = currentdir + 'resources/text_2_ssyk_minified.out.jsonl'
    ads_filepath = currentdir + 'resources/text_2_ssyk.out.jsonl'
    ads = load_ndjson_file(ads_filepath)

    # ads = ads[0:10]

    ads_comparison_datas = []
    for counter, ad in enumerate(ads):
        original_job_posting = jmespath.search('originalJobPosting', ad)

        ad_title = jmespath.search('title', original_job_posting)
        ssyk_text2ssyk = jmespath.search('ssyk_lvl4', ad)
        try:
            ssyk_description_text2ssyk = ssyk_objects[str(ssyk_text2ssyk)]
        except KeyError:
            ssyk_description_text2ssyk = 'Not valid SSYK'

        input_terms = create_input_terms(ad)

        # print(input_terms)
        match_by_text_result = get_match_by_text_result(input_terms, ' ', 1)

        if match_by_text_result and len(match_by_text_result['related_occupations']) > 0:
            related_occupation = match_by_text_result['related_occupations'][0]
            ssyk_match_by_text = jmespath.search('occupation_group.ssyk', related_occupation)
            ssyk_description_match_by_text = jmespath.search('occupation_group.occupation_group_label',
                                                             related_occupation)
            occupation_label_match_by_text = jmespath.search('occupation_label', related_occupation)
        else:
            ssyk_match_by_text = ''
            ssyk_description_match_by_text = ''
            occupation_label_match_by_text = ''

        ad_id = jmespath.search('id', ad)
        comparison_data = {
            "ad_title": ad_title,
            "ssyk_text2ssyk": str(ssyk_text2ssyk),
            "ssyk_description_text2ssyk": ssyk_description_text2ssyk,
            "ssyk_match_by_text": ssyk_match_by_text,
            "ssyk_description_match_by_text": ssyk_description_match_by_text,
            'occupation_label_match_by_text': occupation_label_match_by_text,
            "ad_id": ad_id,
            'ad_url': f'https://arbetsformedlingen.se/platsbanken/annonser/joblinks/{ad_id}'
        }
        ads_comparison_datas.append(comparison_data)

        if counter % 10 == 0:
            print(f'compare - Processed ads: {counter}')
        # print(f'compare - Processed ads: {counter}')

    # print(json.dumps(ads_comparison_datas, indent=4))

    same_ssyk_size = 0
    not_same_ssyk_size = 0
    total_size = len(ads_comparison_datas)
    diffing_comparison_data = []
    for ads_comparison_data in ads_comparison_datas:
        if ads_comparison_data['ssyk_text2ssyk'] == ads_comparison_data['ssyk_match_by_text']:
            same_ssyk_size += 1
        else:
            not_same_ssyk_size += 1
            diffing_comparison_data.append(ads_comparison_data)

    print(f'same_ssyk_size: {same_ssyk_size}, '
          f'not_same_ssyk_size: {not_same_ssyk_size}, '
          f'total_size: {total_size}, '
          f'Percent same: {round(same_ssyk_size / total_size * 100, 2)}%')

    write_diff_to_csv_file('text2ssyk_vs_match_by_text_not_same_ssyk.csv', diffing_comparison_data)


def read_total_arbetsformedlingen_ssyks():
    # ssyk_objects = read_taxonomy_ssyk_objects()
    # ads_filepath = currentdir + 'resources/text_2_ssyk_minified.out.jsonl'
    ads_filepath = currentdir + 'resources/text_2_ssyk.out.jsonl'
    ads = load_ndjson_file(ads_filepath)
    yrkesbenamning_objects = read_taxonomy_yrkesbenamning_objects()

    ad_ssyks = {}
    for counter, ad in enumerate(ads):
        original_job_posting = jmespath.search('originalJobPosting', ad)

        scraper = jmespath.search('scraper', original_job_posting)

        if scraper != 'arbetsformedlingen.se':
            continue

        ad_yrkesbenamning_id = jmespath.search('relevantOccupation.occupationalCategory.codeValue',
                                               original_job_posting)
        try:
            yrkesbenamning_object = yrkesbenamning_objects[ad_yrkesbenamning_id]
            ad_ssyk = jmespath.search('ssyk_code_2012', yrkesbenamning_object)

            if not ad_ssyk in ad_ssyks:
                ad_ssyks[ad_ssyk] = 0
            ad_ssyks[ad_ssyk] = ad_ssyks[ad_ssyk] + 1
        except KeyError:
            log.error(f'Could not find yrkesbenämning id: {ad_yrkesbenamning_id}. Skipping ad')
            continue

    return ad_ssyks


def write_sum_group_by_original_ssyk(total_arbetsformedlingen_ssyks, filename_write):
    ssyk_objects = read_taxonomy_ssyk_objects()

    most_common_ssyks = Counter(total_arbetsformedlingen_ssyks).most_common()

    data_to_save = []
    for tuple in most_common_ssyks:
        unique_ssyk = tuple[0]
        unique_ssyk_count = tuple[1]

        print(f'ssyk: {unique_ssyk}, count: {unique_ssyk_count}')

        try:
            ssyk_description = ssyk_objects[str(unique_ssyk)]
        except KeyError:
            ssyk_description = 'Not valid SSYK'

        # ssyk_total_count = total_arbetsformedlingen_ssyks[unique_ssyk]
        data_to_save.append({
            "ssyk": unique_ssyk,
            "ssyk_description": ssyk_description,
            "ssyk_count": unique_ssyk_count,
            'ssyk_count %': round(unique_ssyk_count / sum(total_arbetsformedlingen_ssyks.values()) * 100, 2)
        })

    keys_for_column_names = [key for key in data_to_save[0].keys()]
    filepath_result_not_same = currentdir + filename_write
    with open(filepath_result_not_same, 'w', newline='', encoding='utf-8-sig') as output_file:
        dict_writer = csv.DictWriter(output_file, keys_for_column_names, dialect='excel', delimiter=';')
        dict_writer.writeheader()

        dict_writer.writerows(data_to_save)
        print(f'Wrote file {filepath_result_not_same}')


def write_sum_group_by_ssyk(filename_read, ssyk_column_name, filename_write, total_arbetsformedlingen_ssyks, is_same=True):
    ssyk_objects = read_taxonomy_ssyk_objects()

    filepath = currentdir + filename_read

    with open(filepath, "r", encoding='utf-8', newline='') as f:
        csv_reader = csv.DictReader(f, delimiter=';')
        records = list(csv_reader)
        classified_ssyks = [record[ssyk_column_name] for record in records]
        most_common_ssyks = Counter(classified_ssyks).most_common()

        data_to_save = []
        for tuple in most_common_ssyks:
            unique_ssyk = tuple[0]
            unique_ssyk_count = tuple[1]

            print(f'ssyk: {unique_ssyk}, count: {unique_ssyk_count}')

            try:
                ssyk_description = ssyk_objects[str(unique_ssyk)]
            except KeyError:
                ssyk_description = 'Not valid SSYK'

            col_name_count = 'correct count' if is_same else 'error count'
            col_name_percent = 'correct %' if is_same else 'error %'
            ssyk_total_count = total_arbetsformedlingen_ssyks[unique_ssyk]
            data_to_save.append({
                "ssyk": unique_ssyk,
                "ssyk_description": ssyk_description,
                col_name_count: unique_ssyk_count,
                "ssyk_total_count": ssyk_total_count,
                col_name_percent: round(unique_ssyk_count / ssyk_total_count * 100, 2)
            })

        keys_for_column_names = [key for key in data_to_save[0].keys()]
        filepath_result_not_same = currentdir + filename_write
        with open(filepath_result_not_same, 'w', newline='', encoding='utf-8-sig') as output_file:
            dict_writer = csv.DictWriter(output_file, keys_for_column_names, dialect='excel', delimiter=';')
            dict_writer.writeheader()

            dict_writer.writerows(data_to_save)
            print(f'Wrote file {filepath_result_not_same}')

def details_for_arbetsformedlingen_ssyk(filename_read, ssyk_to_check, ssyk_column_name):
    # Läs in not same
    ssyk_objects = read_taxonomy_ssyk_objects()

    filepath = currentdir + filename_read

    with open(filepath, "r", encoding='utf-8', newline='') as f:
        csv_reader = csv.DictReader(f, delimiter=';')
        records = list(csv_reader)
        classified_ssyks = [record[ssyk_column_name] for record in records if record['ad_ssyk'] == ssyk_to_check]

        most_common_ssyks = Counter(classified_ssyks).most_common()

        original_ssyk_description = ssyk_objects[ssyk_to_check]

        data_to_save = []
        for tuple in most_common_ssyks:
            unique_ssyk = tuple[0]
            unique_ssyk_count = tuple[1]
            # print(f'ssyk: {unique_ssyk}, count: {unique_ssyk_count}')
            try:
                ssyk_description = ssyk_objects[str(unique_ssyk)]
            except KeyError:
                ssyk_description = 'Not valid SSYK'

            data_to_save.append({
                "original_ssyk": ssyk_to_check,
                "original_ssyk_description": original_ssyk_description,
                "estimated_ssyk": unique_ssyk,
                "ssyk_description": ssyk_description,
                "count": unique_ssyk_count
            })
        filename_write = f'details_for_arbetsformedlingen_ssyk_{ssyk_to_check}.csv'

        keys_for_column_names = [key for key in data_to_save[0].keys()]
        filepath_result_not_same = currentdir + filename_write
        with open(filepath_result_not_same, 'w', newline='', encoding='utf-8-sig') as output_file:
            dict_writer = csv.DictWriter(output_file, keys_for_column_names, dialect='excel', delimiter=';')
            dict_writer.writeheader()

            dict_writer.writerows(data_to_save)
            print(f'Wrote file {filepath_result_not_same}')


def print_input_for_ad(ad_id):

    # ssyk_objects = read_taxonomy_ssyk_objects()
    # ads_filepath = currentdir + 'resources/text_2_ssyk_minified.out.jsonl'
    # ads_filepath = currentdir + 'resources/text_2_ssyk_minified2.out.jsonl'
    ads_filepath = currentdir + 'resources/text_2_ssyk.out.jsonl'
    ads = load_ndjson_file(ads_filepath)

    ad_to_create_input_for = None

    for ad in ads:
        if ad['id'] == ad_id:
            ad_to_create_input_for = ad
            break

    if ad_to_create_input_for:
        input_terms = create_input_terms(ad_to_create_input_for)
        print(f'Input terms:\n{input_terms}')
    else:
        print(f'Could not find ad with id {ad_id}')


# compare()
# compare_only_arbetsformedlingen_se()


# total_arbetsformedlingen_ssyks = read_total_arbetsformedlingen_ssyks()
# write_sum_group_by_ssyk('arbetsformedlingense_vs_text2ssyk_not_same_ssyk.csv', 'ad_ssyk', 'group_ssyk_text2ssyk_not_same.csv', total_arbetsformedlingen_ssyks, is_same=False)
# write_sum_group_by_ssyk('arbetsformedlingense_vs_text2ssyk_same_ssyk.csv', 'ad_ssyk', 'group_ssyk_text2ssyk_same.csv', total_arbetsformedlingen_ssyks, is_same=True)
#
# write_sum_group_by_ssyk('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', 'ad_ssyk', 'group_ssyk_match_by_text_not_same.csv', total_arbetsformedlingen_ssyks, is_same=False)
# write_sum_group_by_ssyk('arbetsformedlingense_vs_match_by_text_same_ssyk.csv', 'ad_ssyk', 'group_ssyk_match_by_text_same.csv', total_arbetsformedlingen_ssyks, is_same=True)
#
# write_sum_group_by_original_ssyk(total_arbetsformedlingen_ssyks, 'arbetsformedlingense_group_by_original_ssyk.csv')
#
# details_for_arbetsformedlingen_ssyk('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', '2221', 'ssyk_match_by_text')
# details_for_arbetsformedlingen_ssyk('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', '3322', 'ssyk_match_by_text')
# details_for_arbetsformedlingen_ssyk('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', '5242', 'ssyk_match_by_text')
# details_for_arbetsformedlingen_ssyk('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', '2512', 'ssyk_match_by_text')
# details_for_arbetsformedlingen_ssyk('arbetsformedlingense_vs_match_by_text_not_same_ssyk.csv', '9520', 'ssyk_match_by_text')
#


# print_input_for_ad('3f6c3af821110f126847bb8cc141ff6f')
# print_input_for_ad('579e7214d45a92994fbd345ef7393141')
# print_input_for_ad('aedfef40e98dea88142633d3facc16a6')
# print_input_for_ad('982ec6aa41631bd785f7cb1bbc0d72ed')
# print_input_for_ad('4b409301f01cbd6bfd7cc0036052b378')
# print_input_for_ad('8d1694de05ae9d2803ccb710050d275e')
# print_input_for_ad('0a186c953842b2e5ea2ccbe4c759eb44')
