import json
import requests
import dataresearch.settings
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

def match_occupation_by_susa_id(susa_id):
    uri = f'{dataresearch.settings.EDUCATION_API_URL}/occupations/match-by-education'
    query = {'education_id': susa_id, 'limit': 1, 'offset': 0, 'include_metadata': False}
    # log.info(f'Match using uri {uri} and query {query}')
    response = requests.get(uri, params=query)
    if response and response.content:
        json_response = json.loads(response.content)
        return json_response
    else:
        return None