import json
import sys
import os
from susa_helpers import description_missing

"""
input file:
A complete scraping with education-scraping/arbetsformedlingen/spiders/susanavet.py
with SKIP_ITEMS_FORMAT = False
default input file:
../resources/susa-all-15nov.json
"""

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def get_desc_frequencies(edu_list):
    freqs = {'no': 0, 'swe': 0, 'eng': 0, 'both': 0}
    for edu in edu_list:
        if description_missing(edu):
            freqs['no'] += 1
        else:
            desc = edu.get('education').get('description')
            langs = [d.get('lang') for d in desc]
            if len(langs) == 0:
                freqs['no'] += 1
            elif len(langs) == 1:
                freqs[langs[0]] += 1
            if 'swe' in langs and 'eng' in langs:
                freqs['both'] += 1
    return freqs


def get_providers_frequencies(edu_list):
    provid_freqs = {}
    for edu in edu_list:
        ps = edu.get('education_providers')
        num_ps = len(ps)
        if not provid_freqs.get(num_ps):
            provid_freqs[num_ps] = 0
        provid_freqs[num_ps] += 1
    return provid_freqs


def get_num_voc(edu_list):
    num_voc = 0
    for edu in edu_list:
        if edu.get('education').get('isVocational'):
            num_voc += 1
    return num_voc


def start():
    if len(sys.argv) >= 2:
        susanavet_filepath = sys.argv[1]
    else:
        susanavet_filepath = currentdir + '../resources/susa-all-26nov.json'
    with open(susanavet_filepath, 'r', encoding='utf-8') as file:
        edu_list = json.load(file)
    print("Total =", len(edu_list))
    print("Number of isVocational:",get_num_voc(edu_list))
    print("Availabilities of descriptions:")
    print(get_desc_frequencies(edu_list))
    print("Frequencies of number of providers:")
    print(dict(sorted(get_providers_frequencies(edu_list).items())))


if __name__ == '__main__':
    start()
