import json
import sys
import os
from susa_helpers import description_missing

"""
input file:
A complete scraping with education-scraping/arbetsformedlingen/spiders/susanavet.py
with SKIP_ITEMS_FORMAT = False
default input file:
../resources/susa-all-15nov.json
"""

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def percent(part, total):
    return int(1000 * part / total) / 10


def get_edu_tree_freqs(edu_tree):
    def get_skola(name, lis):
        num_missing = 0
        for edu in lis:
            if description_missing(edu):
                num_missing += 1

        return (count := len(lis)), num_missing, f"\t\t{name}: {percent(num_missing, count)}%, {num_missing} of {count}\n"
        #return (count := len(lis)), num_missing, ''  # use this for shorter summary

    def get_skoltyp(name, dic):
        list_skolor = []
        for skola in dic:
            list_skolor.append(get_skola(skola, dic[skola]))
        text = ""
        count = 0
        num_missing = 0
        ## Sorting primarily by percentage, and secondly by amount
        for tup in sorted(list_skolor, key=lambda t: t[0] + 1000000 * t[1] / t[0], reverse=True):
            count += tup[0]
            num_missing += tup[1]
            text += tup[2]
        text = f"\t{name}: {percent(num_missing, count)}%, {num_missing} of {count}\n" + text
        return count, num_missing, text

    def get_myndighet(name, dic):
        text = ""
        count = 0
        num_missing = 0
        for skoltyp in dic:
            c, n, t = get_skoltyp(skoltyp, dic[skoltyp])
            count += c
            num_missing += n
            text += t
        text = f"{name}: {percent(num_missing, count)}%, {num_missing} of {count}\n" + text
        return count, num_missing, text

    text = ""
    count = 0
    num_missing = 0
    for myndighet in edu_tree:
        c, n, t = get_myndighet(myndighet, edu_tree[myndighet])
        count += c
        num_missing += n
        text += t
    return count, num_missing, text


def get_edu_tree(edu_list):
    edu_tree = {}
    for edu in edu_list:
        myndighet = edu.get('id').split('.')[1]
        if not edu_tree.get(myndighet):
            edu_tree[myndighet] = []
        edu_tree[myndighet].append(edu)
    for myndighet in edu_tree:
        edu_tree_myndighet_list = edu_tree[myndighet]
        edu_tree[myndighet] = {}
        for edu in edu_tree_myndighet_list:
            skoltyp = edu.get('education', {}).get('form', {}).get('code', {})
            if not edu_tree[myndighet].get(skoltyp):
                edu_tree[myndighet][skoltyp] = []
            edu_tree[myndighet][skoltyp].append(edu)
        for skoltyp in sorted(edu_tree[myndighet]):
            edu_tree_myndighet_skoltyp_list = edu_tree[myndighet][skoltyp]
            edu_tree[myndighet][skoltyp] = {}
            for edu in edu_tree_myndighet_skoltyp_list:
                ## Providers isn't handled fully correctly since there can be many providers, but 99% have only one provider.
                skola = edu.get('education_providers')[0].get('responsibleBody')[0].get('content') or "-"
                if not edu_tree[myndighet][skoltyp].get(skola):
                    edu_tree[myndighet][skoltyp][skola] = []
                edu_tree[myndighet][skoltyp][skola].append(edu)
    return edu_tree


def start():
    if len(sys.argv) >= 2:
        susanavet_filepath = sys.argv[1]
    else:
        susanavet_filepath = currentdir + '../resources/susa-all-26nov.json'
    with open(susanavet_filepath, 'r', encoding='utf-8') as file:
        edu_list = json.load(file)
    edu_tree = get_edu_tree(edu_list)
    # with open('edu_tree.json', 'w', encoding='utf-8') as file: json.dump(edu_tree, file)
    count, num_missing, text = get_edu_tree_freqs(edu_tree)
    print(f"Share of educations with missing 'description': {percent(num_missing, count)}%, {num_missing} of {count}")
    print(text)


if __name__ == '__main__':
    start()
