import copy
import json
import logging
import os
from operator import itemgetter
from ssl import create_default_context

import certifi
import itertools
import jmespath
import requests
from elasticsearch import Elasticsearch
from education_opensearch.opensearch_store import OpensearchStore

import settings

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'


def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])

def get_enrich_result(batch_indata, jae_api_endpoint_url, timeout):
    # print('Getting result from endpoint: %s' % jae_api_endpoint_url)

    headers = {'Content-Type': 'application/json', 'api-key': settings.JAE_API_KEY}

    try:
        r = requests.post(url=jae_api_endpoint_url, headers=headers, json=batch_indata, timeout=timeout)
        r.raise_for_status()
    except Exception as e:
        print(e)
        raise
    else:  # no error
        return r.json()



def get_es_client():
    log.info("Using Elasticsearch node at %s:%s" % (settings.ES_HOST, settings.ES_PORT))

    if settings.ES_USER and settings.ES_PWD:
        context = create_default_context(cafile=certifi.where())
        es = Elasticsearch([settings.ES_HOST], port=settings.ES_PORT,
                           use_ssl=True, scheme='https', ssl_context=context,
                           http_auth=(settings.ES_USER, settings.ES_PWD), timeout=20)
    else:
        es = Elasticsearch([{'host': settings.ES_HOST, 'port': settings.ES_PORT}], timeout=20)

    return es

def get_enriched_susa_education_from_elastic(education_id_wildcard):
    es_client = get_es_client()

    query = {
        "wildcard": {
            "id": {
                "value": education_id_wildcard,
                "boost": 1.0,
                "rewrite": "constant_score"
            }
        }
    }
    #
    results = es_client.search(index=settings.ENRICHED_EDUCATIONS_INDEX, query=query, size=1,
                               track_total_hits=True)

    hits = jmespath.search('hits.hits', results)
    if hits:
        return hits[0]['_source']
    else:
        return None


def load_json_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def get_codes_from_mdh_item(item):
    education_type = jmespath.search("education.type", item).lower()

    item_course_code = jmespath.search("education.course_attributes.course_code", item)

    if not item_course_code and education_type == 'fortbildning':
        item_course_code = jmespath.search("education.continuingeducation_attributes.course_code", item)

    item_program_code = jmespath.search("educationplan.program_code", item)

    occasions = jmespath.search('education.occasions', item)
    # print(occasions)
    application_codes = []
    if occasions:
        for occasion in occasions:
            occ_application_code = jmespath.search('application_code', occasion)
            if occ_application_code:
                application_codes.append(occ_application_code)
            # else:
            #     print(json.dumps(item))

            # if str(occ_application_code).lower() == 'mdh-' + str(application_code).lower():
            #     return item
    if not item_course_code and item_program_code:
        code = item_program_code
    else:
        code = item_course_code
    return {'code': code, 'application_codes': application_codes}

susa_navet_cache = None


def get_all_susa_navet_data():
    global susa_navet_cache
    if susa_navet_cache:
        return susa_navet_cache
    filepath = susanavet_filepath
    susa_navet_cache = load_json_file(filepath)
    return susa_navet_cache


mdh_cache = None

def get_all_mdh_data():
    global mdh_cache
    if mdh_cache:
        return mdh_cache
    filepath = mdh_filepath
    mdh_cache = load_json_file(filepath)
    return mdh_cache


def get_doc_headline_input(education):
    sep = ' | '
    headline_values = []

    # education_title = jmespath.search("education.title[*].content", education)
    education_title = jmespath.search("education.title[?lang=='swe'].content", education)
    if education_title:
        headline_values.extend(education_title)

    education_subjects = jmespath.search("education.subject", education)
    #print(education_subjects)
    if education_subjects:
        educations_subjects_items = [subject['name'] for subject in education_subjects if 'name' in subject]
        headline_values.extend(educations_subjects_items)

    events = jmespath.search("events", education)
    if events:
        for event in events:
            if 'extension' in event:
                if 'keywords' in event['extension']:
                    keywords_swe = jmespath.search("extension.keywords[?lang=='swe'].content", event)
                    if keywords_swe:
                        # Some keywords are numeric, like: 2030
                        keywords_swe = [str(keyword) for keyword in keywords_swe]
                        # print('keywords_swe: %s' % keywords_swe)
                        headline_values.extend(keywords_swe)


    doc_headline_input = sep.join(headline_values)

    # print("doc_headline_input: %s" % doc_headline_input)

    return doc_headline_input

def get_doc_description_input(education, course_plan_text):
    education_description = jmespath.search("education.description[?lang=='swe'].content | [0]", education)

    if education_description:
        education_description = education_description + '\n' + course_plan_text
    else:
        education_description = course_plan_text

    if not education_description:
        education_description = ''

    # print("doc_description_input: %s" % education_description)

    return education_description


def clean_enriched_values(enriched_output):
    enriched_candidates = enriched_output['enriched_candidates']
    for candidatetype in enriched_candidates.keys():
        candidates = enriched_candidates[candidatetype]
        for candidate in candidates:
            candidate.pop("prediction", None)

def set_unique_candidates(enriched_output):
    enriched_candidates = enriched_output['enriched_candidates']
    for candidatetype in enriched_candidates.keys():
        candidates = enriched_candidates[candidatetype]
        unique_filtered_candidates = {}
        sorted_predictions_asc = sorted(candidates, key=itemgetter('concept_label'), reverse=False)
        for competence in sorted_predictions_asc:
            unique_filtered_candidates[competence['concept_label']] = competence
        unique_filtered_candidates_list = [val for key, val in unique_filtered_candidates.items()]
        enriched_output['enriched_candidates'][candidatetype] = unique_filtered_candidates_list


def clean_enriched_result(doc_id, enriched_results_data):

        enriched_output = enriched_results_data
        set_unique_candidates(enriched_output)
        clean_enriched_values(enriched_output)

        enriched_output.pop(ENRICHER_PARAM_DOC_ID, None)
        enriched_output.pop(ENRICHER_PARAM_DOC_HEADLINE, None)

        return enriched_output

def get_empty_output_value():
    return {
        "enriched_candidates": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        }
    }

def compare_competences_count_course_plans(write_avg_to_json_file=False, write_course_plan_enriched_educations_to_elastic=False):
    mdh_json_data = get_all_mdh_data()
    print('len(mdh_json_data): %s' % len(mdh_json_data))

    # max_items = 2
    max_items = None
    item_counter = 0

    jae_api_endpoint_url = settings.JAE_API_URL + '/enrichtextdocuments'
    print('Getting result from endpoint: %s' % jae_api_endpoint_url)

    enriched_courses_list = []

    susa_ids = []

    for mdh_item in mdh_json_data:
        mdh_codes = get_codes_from_mdh_item(mdh_item)
        # print(mdh_codes)
        mdh_code = mdh_codes['code']
        if mdh_code:
            mdh_code = mdh_code.lower()
            mdh_appl_codes = mdh_codes['application_codes']
            if len(mdh_appl_codes) > 0:
                for appl_code in mdh_appl_codes:
                    clean_appl_code = appl_code.replace('MDH-', '')

                    print('mdh, url: %s' % mdh_item['education']['url'])
                    print('mdh_code: %s - clean_appl_code: %s' % (mdh_code, clean_appl_code))

                    # education_id_wildcard = 'i.uoh.mdh.mma501.21171.20212'
                    education_id_wildcard = 'i.uoh.mdh.%s.%s.*' % (mdh_code, clean_appl_code)

                    # print(education_id_wildcard)
                    susa_item = get_enriched_susa_education_from_elastic(education_id_wildcard)

                    if susa_item:
                        # print(json.dumps(susa_item))
                        susa_enriched_candidates = jmespath.search('text_enrichments_results.enriched_result.enriched_candidates', susa_item)

                        mdh_courseplan_objectives = jmespath.search('courseplan.objectives', mdh_item)
                        if not mdh_courseplan_objectives:
                            mdh_courseplan_objectives = ''
                        mdh_courseplan_learning_outcomes = jmespath.search('courseplan.learning_outcomes', mdh_item)
                        if not mdh_courseplan_learning_outcomes:
                            mdh_courseplan_learning_outcomes = ''
                        mdh_courseplan_course_content = jmespath.search('courseplan.course_content', mdh_item)
                        if not mdh_courseplan_course_content:
                            mdh_courseplan_course_content = ''
                        mdh_courseplan_text = mdh_courseplan_objectives + '\n' + mdh_courseplan_learning_outcomes + '\n' + mdh_courseplan_course_content

                        susa_id = str(susa_item.get('id', ''))

                        doc_id = susa_id
                        doc_headline = get_doc_headline_input(susa_item)
                        doc_text = get_doc_description_input(susa_item, mdh_courseplan_text)

                        input_doc_params = {
                            ENRICHER_PARAM_DOC_ID: doc_id,
                            ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
                            ENRICHER_PARAM_DOC_TEXT: doc_text
                        }

                        ad_batches = grouper(100, [input_doc_params])

                        batch_indata_config = {
                            "include_terms_info": True,
                            "include_sentences": False,
                            # "sort_by_prediction_score": "DESC"
                            "sort_by_prediction_score": "NOT_SORTED"
                        }

                        ad_batch_indatas = [ad_indata for ad_indata in ad_batches]
                        batch_indata = copy.deepcopy(batch_indata_config)
                        batch_indata['documents_input'] = ad_batch_indatas[0]



                        enrich_results_data = get_enrich_result(batch_indata, jae_api_endpoint_url, settings.JAE_API_TIMEOUT_SECONDS)
                        # print(json.dumps(enrich_results_data))

                        susa_plus_course_plan_enriched_result = clean_enriched_result(susa_id, enrich_results_data[0])
                        susa_plus_course_plan_enriched_candidates = susa_plus_course_plan_enriched_result['enriched_candidates']


                        clean_susa_item = copy.deepcopy(susa_item)
                        clean_susa_item.pop('text_enrichments_results', None)

                        susa_nr_enriched_competencies = len(jmespath.search('competencies', susa_enriched_candidates))
                        susa_plus_course_plan_nr_enriched_competencies = len(jmespath.search('competencies', susa_plus_course_plan_enriched_candidates))
                        item = {"id": susa_id,
                                "mdh_courseplan_text": mdh_courseplan_text,
                                "susa_item": clean_susa_item,
                                "susa_nr_enriched_competencies": susa_nr_enriched_competencies,
                                "susa_plus_course_plan_nr_enriched_competencies": susa_plus_course_plan_nr_enriched_competencies,
                                "susa_enriched_candidates": susa_enriched_candidates,
                                "susa_plus_course_plan_enriched_candidates": susa_plus_course_plan_enriched_candidates}

                        if susa_nr_enriched_competencies < susa_plus_course_plan_nr_enriched_competencies:
                            susa_ids.append(susa_id)
                        # print(json.dumps(item))

                        enriched_courses_list.append(item)
                        # print('doc_id: %s \ndoc_headline: %s \n doc_text: %s' % (doc_id, doc_headline, doc_text))

                        # print(json.dumps(mdh_item))


        item_counter += 1
        if max_items and item_counter >= max_items:
            break

    if write_course_plan_enriched_educations_to_elastic:
        #           "text_enrichments_results" : {
        #             "enriched_result" : {
        #               "enriched_candidates" : {
        #                 "occupations" : [

        enriched_courses_to_store = []

        for item in enriched_courses_list:
            course = item['susa_item']
            course['text_enrichments_results'] = {}
            course['text_enrichments_results']['enriched_result'] = {}
            course['text_enrichments_results']['enriched_result']['enriched_candidates'] = item["susa_plus_course_plan_enriched_candidates"]
            enriched_courses_to_store.append(course)

        # print(json.dumps(enriched_courses_to_store))


        elastic_store = OpensearchStore()
        elastic_store.start_new_save('susa-navet-course-plan-enriched')
        elastic_store.save_educations_to_elastic(enriched_courses_to_store)

    if write_avg_to_json_file:
        write_avg_result_to_json_file(enriched_courses_list)

        print(sorted(list(set(susa_ids))))


def write_avg_result_to_json_file(enriched_courses_list):
    susa_competencies_total = 0
    susa_course_plan_competencies_total = 0
    susa_occupations_total = 0
    susa_course_plan_occupations_total = 0
    for course in enriched_courses_list:
        susa_competencies_total += len(jmespath.search('susa_enriched_candidates.competencies', course))
        susa_course_plan_competencies_total += len(
            jmespath.search('susa_plus_course_plan_enriched_candidates.competencies', course))
        susa_occupations_total += len(jmespath.search('susa_enriched_candidates.occupations', course))
        susa_course_plan_occupations_total += len(
            jmespath.search('susa_plus_course_plan_enriched_candidates.occupations', course))
    num_enriched_courses = len(enriched_courses_list)
    round_num_decimals = 2
    susa_navet_avg_nr_competencies = round(susa_competencies_total / num_enriched_courses, round_num_decimals)
    susa_plus_course_plan_avg_nr_competencies = round(susa_course_plan_competencies_total / num_enriched_courses,
                                                      round_num_decimals)
    susa_navet_avg_nr_occupations = round(susa_occupations_total / num_enriched_courses, round_num_decimals)
    susa_plus_course_plan_avg_nr_occupations = round(susa_course_plan_occupations_total / num_enriched_courses,
                                                     round_num_decimals)
    data_to_save = {
        "nr_courses": num_enriched_courses,
        "susa_avg": {
            "nr_competencies": susa_navet_avg_nr_competencies,
            "nr_occupations": susa_navet_avg_nr_occupations
        },
        "susa_plus_course_plan_avg": {
            "nr_competencies": susa_plus_course_plan_avg_nr_competencies,
            "nr_occupations": susa_plus_course_plan_avg_nr_occupations
        },
        "enriched_courses": enriched_courses_list
    }
    # enriched_courses_list
    with open('susa_plus_course_plan_enriched_courses.json', 'w', encoding='utf-8') as f:
        json.dump(data_to_save, f, ensure_ascii=False, indent=4)


# i.uoh.mdh.agm05.43901.20212

# mdh_code: MMA501 - clean_appl_code: 21171
# i.uoh.mdh.mma501.21171.20212

mdh_filepath = currentdir + '../resources/mdh_output_complete_2021-10-13.json'

susanavet_filepath = currentdir + '../resources/susa_navet_output_complete_2021-11-15.json'

compare_competences_count_course_plans(write_avg_to_json_file=True)
# compare_competences_count_course_plans(write_course_plan_enriched_educations_to_elastic=True)

# compare_competences_count_course_plans('i.uoh.mdh.mma501.21171.*')

#     "nr_courses": 1055,
#     "susa_avg": {
#         "nr_competencies": 3.48,
#         "nr_occupations": 0.37
#     },
#     "susa_plus_course_plan_avg": {
#         "nr_competencies": 10.16,
#         "nr_occupations": 0.81
#     },