import json
import logging
import jmespath

from dataresearch import education_api_helper
from dataresearch.minio_downloader import MinioDownloader

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

sun_codes = {}
none_sun_codes = {}

def sun_code_parser():
    log.debug('Starting to download education file from minio...')
    minio_downloader = MinioDownloader()

    best_match_file = minio_downloader.find_best_match_file()

    educations_with_subject_codes = 0
    educations_with_no_subject_code = 0
    number_of_codes = 0

    log.debug('Parse educations and determine sun codes in subject field...')

    for education in minio_downloader.download_educations(best_match_file):
        has_subject_code = False
        education_subjects = jmespath.search("education.subject", education)
        if education_subjects:
            for education_subject in education_subjects:
                subject_code = jmespath.search("code", education_subject)
                susa_id = jmespath.search("id", education)

                if subject_code:
                    subject_code = str(subject_code).strip()

                if subject_code:
                    has_subject_code = True
                    number_of_codes +=1


                    if len(subject_code) >= 6:
                        educations_with_subject_codes +=1
                        if sun_codes.get(subject_code):
                            subject_info = sun_codes[subject_code]
                            subject_info['number_of_occurrences'] = subject_info.get('number_of_occurrences') + 1

                            if susa_id:
                                subject_info['susa_ids'].append(susa_id)

                            sun_codes[subject_code] = subject_info

                        else:
                            subject_info = {'number_of_occurrences': 1}

                            if susa_id:
                                subject_info['susa_ids'] = [susa_id]
                            else:
                                subject_info['susa_ids'] = []

                            sun_codes[subject_code] = subject_info

                    else:
                        educations_with_no_subject_code
                        if none_sun_codes.get(subject_code):
                            subject_info = none_sun_codes[subject_code]
                            subject_info['number_of_occurrences'] = subject_info.get('number_of_occurrences') + 1
                            none_sun_codes[subject_code] = subject_info

                        else:
                            subject_info = {'number_of_occurrences': 1}
                            none_sun_codes[subject_code] = subject_info

    for k, v in sun_codes.items():
        top_occupation = None

        susa_ids = v.get('susa_ids')
        for susa_id in susa_ids:
            if susa_id:
                log.info(f'Match occupation by susa_id {susa_id} for sun_code {k}')
                matched_result = education_api_helper.match_occupation_by_susa_id(susa_id)
                if matched_result:
                    top_occupations = matched_result.get('related_occupations')
                    if top_occupations:
                        top_occupation = top_occupations[0]
                        if v.get('top_occupations'):
                            top_occupations_for_subject = v['top_occupations']
                            occupation_found = False
                            for top_occupation_for_subject in top_occupations_for_subject:
                                if top_occupation_for_subject == top_occupation:
                                    occupation_found = True
                            if not occupation_found:
                                v['top_occupations'].append(top_occupation)
                        else:
                            v['top_occupations'] = [top_occupation]

    log.info('Write statistics to file...')

    with open('sun_code_education_mapping.json', 'w', encoding='utf-8') as f:
        for k, v in sun_codes.items():
            json.dump({'sun_code': k, 'occupations': []}, f, ensure_ascii=False, indent=4)

    with open('sun_codes_stat.txt', 'w', encoding='utf-8') as f:
        print('------ Sun codes: ------', file=f)
        for k, v in sun_codes.items():
            print(f'{k}\t{v}', file=f)

        print('------ None sun codes: ------', file=f)
        for k, v in none_sun_codes.items():
            print(f'{k}\t{v}', file=f)

        print('Sun code\tNumber of occurrences', file=f)
        print(f'Number of sun codes:\t{number_of_codes}', file=f)
        print(f'Number of educations with sun codes:\t{educations_with_subject_codes}', file=f)
        print(f'Number of educations with no sun code:\t{educations_with_no_subject_code}', file=f)

sun_code_parser()