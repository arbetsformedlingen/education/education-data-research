import json
import sys
import os
import requests

"""
Only first url of first event of each education is checked. It's already very slow as it is...
And redirects (30x) are not followed. So there can be more failures than shown here.

input file:
A complete scraping with education-scraping/arbetsformedlingen/spiders/susanavet.py
with SKIP_ITEMS_FORMAT = False
default input file:
../resources/susa-all-26nov.json
"""

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def get_url_frequencies(edu_list):
    filename = "url_count_acc.txt"
    file = open(filename, "w")
    file.close()
    url_freqs = {0: 0}
    for i, edu in enumerate(edu_list):
        if not (url_block := edu.get('events')[0].get('urls')) or not (url := url_block[0].get('content')):
            url_freqs[0] += 1
        else:
            if url[:4] != 'http':
                url = 'https://' + url
            try:
                response = requests.head(url, timeout=5)
                status_code = response.status_code
            except:
                status_code = 999
            if not url_freqs.get(status_code):
                url_freqs[status_code] = 0
            url_freqs[status_code] += 1
        print('.', end='')
        if i % 1000 == 0:
            file = open(filename, "a")
            file.write(f"{i}: {url_freqs}\n")
            file.close()
            print(i)
    return url_freqs

def start():
    if len(sys.argv) >= 2:
        susanavet_filepath = sys.argv[1]
    else:
        susanavet_filepath = currentdir + '../resources/susa-all-26nov.json'
    with open(susanavet_filepath, 'r', encoding='utf-8') as file:
        edu_list = json.load(file)
    print("Frequencies for urls by HTTP response code. 0 = no URL, 999 = exception")
    result = get_url_frequencies(edu_list)
    result = dict(sorted(result.items()))
    print(result)
    file = open("url_count_final.txt", "w")
    file.write(str(result))
    file.close()


if __name__ == '__main__':
    start()
