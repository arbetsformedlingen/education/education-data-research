import json
import os
import re

import jmespath
from gensim import utils
from pprint import pprint

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def load_json_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def get_susa_navet_json_item_by_code(course_code, application_code):
    json_data = get_all_susa_navet_data()
    for item in json_data:
        item_course_code = jmespath.search("education.content.educationInfo.code", item)
        if str(item_course_code) == str(course_code):
            events = jmespath.search('events', item)
            for event in events:
                event_application_code = jmespath.search('application.code', event)
                if str(event_application_code) == str(application_code):
                    return item

    return None


def get_mdh_json_item_by_code(course_code, application_code):
    json_data = get_all_mdh_data()
    for item in json_data:
        item_course_code = jmespath.search("education.course_attributes.course_code", item)
        if str(item_course_code) == str(course_code):
            # print(item)
            occasions = jmespath.search('education.occasions', item)
            # print(occasions)
            if occasions:
                for occasion in occasions:
                    occ_application_code = jmespath.search('application_code', occasion)
                    if str(occ_application_code).lower() == 'mdh-' + str(application_code).lower():
                        return item

    return None


def get_codes_from_mdh_item(item):
    education_type = jmespath.search("education.type", item).lower()

    item_course_code = jmespath.search("education.course_attributes.course_code", item)

    if not item_course_code and education_type == 'fortbildning':
        item_course_code = jmespath.search("education.continuingeducation_attributes.course_code", item)

    item_program_code = jmespath.search("educationplan.program_code", item)

    occasions = jmespath.search('education.occasions', item)
    # print(occasions)
    application_codes = []
    if occasions:
        for occasion in occasions:
            occ_application_code = jmespath.search('application_code', occasion)
            if occ_application_code:
                application_codes.append(occ_application_code)
            # else:
            #     print(json.dumps(item))

            # if str(occ_application_code).lower() == 'mdh-' + str(application_code).lower():
            #     return item
    if not item_course_code and item_program_code:
        code = item_program_code
    else:
        code = item_course_code
    return {'code': code, 'application_codes': application_codes}


susa_navet_cache = None


def get_all_susa_navet_data():
    global susa_navet_cache
    if susa_navet_cache:
        return susa_navet_cache
    filepath = susanavet_filepath
    susa_navet_cache = load_json_file(filepath)
    return susa_navet_cache


mdh_cache = None


def get_all_mdh_data():
    global mdh_cache
    if mdh_cache:
        return mdh_cache
    filepath = mdh_filepath
    mdh_cache = load_json_file(filepath)
    return mdh_cache


def get_susa_navet_json_item_by_url(course_code, application_code):
    json_data = get_all_susa_navet_data()
    for item in json_data:
        item_course_code = jmespath.search("education.content.educationInfo.code", item)
        if str(item_course_code) == str(course_code):
            events = jmespath.search('events', item)
            for event in events:
                event_application_code = jmespath.search('application.code', event)
                if str(event_application_code) == str(application_code):
                    return item

    return None


def get_description_from_susa_item(susa_item, lang):
    code = jmespath.search("education.content.educationInfo.code", susa_item)
    susa_description_strs = jmespath.search("education.content.educationInfo.description.string", susa_item)
    if susa_description_strs:
        for susa_description_str in susa_description_strs:
            susa_description_content = jmespath.search("content", susa_description_str)
            susa_description_lang = jmespath.search("lang", susa_description_str)

            if susa_description_lang == lang:
                return susa_description_content

    else:
        print("Item in susa-navet with code %s did not have a description at all" % code)


def check_description_count_diffs():
    mdh_json_data = get_all_mdh_data()
    print('len(mdh_json_data): %s' % len(mdh_json_data))

    # susanavet_json_data = get_all_susa_navet_data()

    navet_items_missing_description = []
    navet_codes_missing_description = []
    navet_total_found_codes = set()

    for mdh_item in mdh_json_data:
        mdh_codes = get_codes_from_mdh_item(mdh_item)
        # print(mdh_codes)
        mdh_code = mdh_codes['code']
        if mdh_code:
            mdh_appl_codes = mdh_codes['application_codes']
            if len(mdh_appl_codes) > 0:
                susa_item_has_any_description = False
                for appl_code in mdh_appl_codes:
                    clean_appl_code = appl_code.replace('MDH-', '')
                    susa_item = get_susa_navet_json_item_by_code(mdh_code, clean_appl_code)

                    if susa_item:
                        susa_description = get_description_from_susa_item(susa_item, 'swe')
                        navet_total_found_codes.add(mdh_code)
                        mdh_description = jmespath.search("education.description", mdh_item)

                        if mdh_description and not susa_description:
                            print(
                                'Item with code: %s is missing susa_description for lang swe but has a description in mdh.se' % (
                                    mdh_code))
                            print('mdh_description: %s \nsusa_description: %s' % (mdh_description, susa_description))
                            navet_items_missing_description.append(susa_item)
                            navet_codes_missing_description.append(mdh_code)

    print('len(navet_items_missing_description): %s' % len(navet_items_missing_description))
    print('navet_codes_missing_description: %s' % navet_codes_missing_description)

    print('len(mdh_json_data): %s' % len(mdh_json_data))
    print('len(navet_total_found_codes): %s' % len(navet_total_found_codes))


import difflib


def check_description_data_diffs():
    mdh_json_data = get_all_mdh_data()
    print('len(mdh_json_data): %s' % len(mdh_json_data))

    description_diffs = []

    for mdh_item in mdh_json_data:
        mdh_codes = get_codes_from_mdh_item(mdh_item)
        # print(mdh_codes)
        mdh_code = mdh_codes['code']
        if mdh_code:
            mdh_appl_codes = mdh_codes['application_codes']
            if len(mdh_appl_codes) > 0:
                susa_item_has_any_description = False
                for appl_code in mdh_appl_codes:
                    clean_appl_code = appl_code.replace('MDH-', '')
                    susa_item = get_susa_navet_json_item_by_code(mdh_code, clean_appl_code)

                    if susa_item:
                        susa_description = get_description_from_susa_item(susa_item, 'swe')
                        mdh_description = jmespath.search("education.description", mdh_item)

                        if mdh_description and susa_description:

                            clean_mdh_description = clean_description(mdh_description)
                            clean_susa_description = clean_description(susa_description)

                            if clean_mdh_description == clean_susa_description:
                                print(':) Description for code %s is identical after cleaning!' % mdh_code)
                            else:
                                print(':( Description for code %s is not identical!' % mdh_code)
                                print('mdh-description: %s' % clean_mdh_description)
                                print('sus-description: %s' % clean_susa_description)
                                text_diff = get_text_diff(clean_mdh_description.splitlines(keepends=True), clean_susa_description.splitlines(keepends=True))

                                description_diffs.append({'code': mdh_code, 'text_diff': text_diff})


    print(json.dumps(description_diffs))
    # pprint((description_diffs))

# Ta bort whitespace förutom radbrytningar
RE_WHITESPACE_LOCAL = re.compile(r"([\t ])+", re.UNICODE)

def strip_multiple_whitespaces_local(s, **kwargs):
    s = utils.to_unicode(s)
    return RE_WHITESPACE_LOCAL.sub(" ", s)

def clean_description(description):
    unistring = utils.to_unicode(description)
    cleaned = unistring.replace(u'\u00A0', ' ')
    cleaned = cleaned.replace('\n', ' ')
    cleaned = strip_multiple_whitespaces_local(cleaned)
    cleaned = cleaned.replace('. ', '.')
    cleaned = cleaned.replace('? ', '?')
    cleaned = cleaned.replace(': ', ':')



    return cleaned

def get_text_diff(str1, str2):
    d = difflib.Differ()
    diff = list(d.compare(str1, str2))
    # pprint('\n'.join(list(diff)))
    return '\n'.join(diff)


def print_text_diff(str1, str2):
    print(get_text_diff(str1, str2))


# test_str1 = '''Ansvarsfull innovation innebär att söka vilka förbättringar som är bäst för samhället och miljön på lokal, regional och global nivå. Ansvarsfull innovation tar hänsyn till den effekt som nya tjänster, varor, beteenden, processer eller affärsmodeller kan ha på samhället, ekonomin och miljön. Detta innebär att ett ansvarsfullt förhållningssätt till innovation avser att skapa förändringar som har en positiv inverkan på såväl samhället och ekonomin som på miljön och klimatet.
# Ansvarsfull innovation och forskning (RRI) är ett delområde inom Ansvarsfull innovation och har utvecklats som ett styrmedel för skattefinansierade forsknings och innovationsprojekt inom EU. Det vill säga, här är även forskning inkluderat. RRI innebär att samhällsaktörer arbetar tillsammans under hela forsknings- och innovationsprocessen för att anpassa både processen och dess resultat, till samhällets värderingar, behov och förväntningar. RRI är ett ramverk för att skapa en forsknings- och innovationspraktik som drivs av samhällets behov och engagerar alla samhällsaktörer via inkluderande metoder.
# Det här området har två kurser; Ansvarsfull forskning och innovation 2,5 hp samt Ansvarsfull innovation 5 hp.
# I den här kursen får du en översikt om området Ansvarsfull forskning och innovation (RRI), du får testa perspektivet och dess metoder på case, och du får utforska RRIs verktygslåda RRI-tools.'''
# test_str2 = '''Ansvarsfull innovation innebär att söka vilka förbättringar som är bäst för samhället och miljön på lokal, regional och global nivå. Ansvarsfull innovation tar hänsyn till den effekt som nya tjänster, varor, beteenden, processer eller affärsmodeller kan ha på samhället, ekonomin och miljön. Detta innebär att ett ansvarsfullt förhållningssätt till innovation avser att skapa förändringar som har en positiv inverkan på såväl samhället och ekonomin som på miljön och klimatet.  Ansvarsfull innovation och forskning (RRI) är ett delområde inom Ansvarsfull innovation och har utvecklats som ett styrmedel för skattefinansierade forsknings och innovationsprojekt inom EU. Det vill säga, här är även forskning inkluderat. RRI innebär att samhällsaktörer arbetar tillsammans under hela forsknings- och innovationsprocessen för att anpassa både processen och dess resultat, till samhällets värderingar, behov och förväntningar. RRI är ett ramverk för att skapa en forsknings- och innovationspraktik som drivs av samhällets behov och engagerar alla samhällsaktörer via inkluderande metoder.  Det här området har två kurser; Ansvarsfull forskning och innovation 2,5 hp samt Ansvarsfull innovation 5 hp. I den här kursen får du en översikt om området Ansvarsfull forskning och innovation (RRI), du får testa perspektivet och dess metoder på case, och du får utforska RRIs verktygslåda RRI-tools.'''
#
# test_str1 = clean_description(test_str1).splitlines(keepends=True)
# test_str2 = clean_description(test_str2).splitlines(keepends=True)
#
# print_text_diff(test_str1, test_str2)

# Questions:
# What fields/attributes in susa-navet contains competencies terms? (for matching the labor market)
# Answer: title, description
# What fields/attributes in mdh.se contains competencies terms? (for matching the labor market)
# Answer: education.description, education.title, courseplan.objectives, courseplan.learning_outcomes, courseplan.learning_outcomes, courseplan.course_content

# Is any data different or missing in susa-navet compared to the data in mdh.se, for example texts that contains competencies terms?
# What field/attributes in mdh.se are missing in susa-navet/EMIL-standard (for matching the labor market)?

# mdh_filepath = currentdir + '../resources/mdh_output.json'
mdh_filepath = currentdir + '../resources/mdh_output_complete_2021-10-13.json'
# susanavet_filepath = currentdir + '../resources/susa_navet_output_200_items.json'
susanavet_filepath = currentdir + '../resources/susa_navet_output_complete_2021-10-04.json'

# print_json_structure(mdh_filepath)
# print_json_structure(susanavet_filepath)


# check_description_count_diffs()

check_description_data_diffs()


