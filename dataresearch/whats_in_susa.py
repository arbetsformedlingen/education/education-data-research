import json
import os
import sys
from susa_helpers import education_info

"""
input file:
A complete scraping with education-scraping/arbetsformedlingen/spiders/susanavet.py
with SKIP_ITEMS_FORMAT = False
default input file:
../resources/susa-all-15nov.json
"""

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'



def susa_investigate(edu_list):
    nr_of_records = len(edu_list)
    print(nr_of_records)
    matrix = [[[[0 for has_fee in range(2)] for no_csn in range(2)]
              for konst_kultur in range(2)] for endast_tillsyn in range(2)]
    for edu in edu_list:
        no_csn = edu.get('education').get('eligibleForStudentAid', {}).get('code') == "nej"
        form_code = edu.get('education').get('form').get('code')
        konst_kultur = form_code == "konst- och kulturutbildning"
        endast_tillsyn = form_code == "utbildning med endast tillsyn"
        has_fee = False
        # Mixed fee: only 'i.myh.8195', 'Tandsköterska'
        for event in edu.get('events'):
            if (has_fee := not event.get('fee') is None):
                break
        matrix[has_fee][no_csn][konst_kultur][endast_tillsyn] += 1
        #if endast_tillsyn and has_fee: print(education_info(edu))
    print("[[-, et], [kk, kk+et]]")
    for hf, m1 in enumerate(matrix):
        for nc, m2 in enumerate(m1):
            print(f"has_fee={hf}, no_csn={nc}: {m2}")


def start():
    if len(sys.argv) >= 2:
        susanavet_filepath = sys.argv[1]
    else:
        susanavet_filepath = currentdir + '../resources/susa-all-26nov.json'
    with open(susanavet_filepath, 'r', encoding='utf-8') as file:
        edu_list = json.load(file)
    susa_investigate(edu_list)


if __name__ == '__main__':
    start()
