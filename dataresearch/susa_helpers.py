import re
from bs4 import BeautifulSoup
import warnings

warnings.filterwarnings("ignore", category=UserWarning, module='bs4')


def education_info(edu):
    return (edu.get('id'),
            edu.get('education').get('title')[0].get('content'),
            edu.get('education_providers')[0].get('responsibleBody')[0].get('content')
            )


def description_missing(edu):
    """
    Identifies if an education is missing its description, i.e. if both 'swe' and 'eng' are empty.
    Also cases such as these are counted as empty:
        "<p></p>"
        "."
        "https://someurltothecorse"
    """
    desc_list = edu.get('education').get('description')
    no_desc = True
    for desc in desc_list:
        clean_text = clean_html(desc.get('content'))
        clean_text = re.sub('http[s]?://\S+', '', clean_text)  # remove URLs
        if len(clean_text) > 1:  # sometimes it's just "."
            no_desc = False
    return no_desc


tags_display_block = ['p', 'div', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']


def clean_html(text):
    if text is None:
        cleaned_text = ''
    else:
        soup = BeautifulSoup(text, 'html.parser')
        # Remove all script-tags
        [s.extract() for s in soup('script')]

        for tag_name in tags_display_block:
            _add_linebreak_for_tag_name(tag_name, '\n', '\n', soup)

        _add_linebreak_for_tag_name('li', '', '\n', soup)
        _add_linebreak_for_tag_name('br', '', '\n', soup)

        cleaned_text = soup.get_text()

        cleaned_text = cleaned_text.strip()

    return cleaned_text


def _add_linebreak_for_tag_name(tag_name, replacement_before, replacement_after, soup):
    parent_tags = soup.find_all(tag_name, recursive=True)

    for tag in parent_tags:
        if replacement_before:
            previous_sibling = tag.find_previous_sibling()

            if not previous_sibling or \
                    (previous_sibling and previous_sibling.name not in tags_display_block):
                tag.insert_before(replacement_before)
        if replacement_after:
            tag.insert_after(replacement_after)
