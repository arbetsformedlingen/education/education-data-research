import os

ENRICH_CALLS_PARALLELISM = int(os.getenv('ENRICH_CALLS_PARALLELISM', '8'))
# JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-api.jobtechdev.se')
JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-test-api.jobtechdev.se')
JAE_API_TIMEOUT_SECONDS = int(os.getenv('JAE_API_TIMEOUT_SECONDS', '60'))

JAE_API_KEY = os.getenv('JAE_API_KEY', 'your api key here')
EDUCATION_API_URL = os.getenv('EDUCATION_API_URL', 'https://education-api-test.jobtechdev.se')

# ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211025-17.08")

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")

VERSION = '0.0.1'

ES_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations-course-plan-enriched')
education_mappings = {
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            }
        }
    }
}

# ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211025-17.08")
# ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211116-10.16")
ENRICHED_EDUCATIONS_INDEX = os.getenv("ENRICHED_EDUCATIONS_INDEX", "educations-enriched-susa-navet-enriched-20211116-11.33")

S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'kll-test')
AWS_ACCESS_KEY = os.environ.get('AWS_ACCESS_KEY', 'kll')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'abcd1234')
S3_URL = os.environ.get('S3_URL', 'http://localhost:19000')
MINIO_TO_OPENSEARCH_S3_EDUCATIONS_SRC_FILE_PREFIX = os.getenv('MINIO_TO_OPENSEARCH_S3_EDUCATIONS_SRC_FILE_PREFIX', 'merged_and_enriched_educations')
MINIO_TO_OPENSEARCH_S3_SRC_FILE_MAX_VALID_HOURS = os.getenv('MINIO_TO_OPENSEARCH_S3_SRC_FILE_MAX_VALID_HOURS', '300')

