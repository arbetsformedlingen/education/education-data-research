import json
import os
import re

import jmespath
import pandas as pd
import requests

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def load_json_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def get_json_item_by_code(course_code, jmesquery, filepath):
    json_data = load_json_file(filepath)
    for item in json_data:
        item_course_code = jmespath.search(jmesquery, item)
        if item_course_code == course_code:
            return item

    return None


def get_susa_navet_json_item_by_code(course_code, application_code, filepath):
    json_data = load_json_file(filepath)
    for item in json_data:
        item_course_code = jmespath.search("education.content.educationInfo.code", item)
        if str(item_course_code) == str(course_code):
            events = jmespath.search('events', item)
            for event in events:
                event_application_code = jmespath.search('application.code', event)
                if str(event_application_code) == str(application_code):
                    return item

    return None


susa_navet_cache = None


def get_all_susa_navet_data():
    global susa_navet_cache
    if susa_navet_cache:
        return susa_navet_cache
    filepath = susanavet_filepath
    susa_navet_cache = load_json_file(filepath)
    return susa_navet_cache


mdh_cache = None


def get_all_mdh_data():
    global mdh_cache
    if mdh_cache:
        return mdh_cache
    filepath = mdh_filepath
    mdh_cache = load_json_file(filepath)
    return mdh_cache


def get_susa_navet_json_item_by_url(course_code, application_code):
    json_data = get_all_susa_navet_data()
    for item in json_data:
        item_course_code = jmespath.search("education.content.educationInfo.code", item)
        if str(item_course_code) == str(course_code):
            events = jmespath.search('events', item)
            for event in events:
                event_application_code = jmespath.search('application.code', event)
                if str(event_application_code) == str(application_code):
                    return item

    return None


def get_mdh_json_item_by_code(course_code, application_code):
    json_data = get_all_mdh_data()
    for item in json_data:
        item_course_code = jmespath.search("education.course_attributes.course_code", item)
        if str(item_course_code) == str(course_code):
            # print(item)
            occasions = jmespath.search('education.occasions', item)
            # print(occasions)
            if occasions:
                for occasion in occasions:
                    occ_application_code = jmespath.search('application_code', occasion)
                    if str(occ_application_code).lower() == 'mdh-' + str(application_code).lower():
                        return item

    return None


def print_original_items_by_code(course_code, application_code, the_susanavet_filepath, the_mdh_filepath):
    susa_navet_item = get_susa_navet_json_item_by_code(course_code, application_code, the_susanavet_filepath)
    print(('#' * 40) + '\nsusa-navet\n' + ('#' * 40))
    print(json.dumps(susa_navet_item))

    mdh_item = get_mdh_json_item_by_code(course_code, application_code)
    print(('#' * 40) + '\nmdh\n' + ('#' * 40))
    print(json.dumps(mdh_item))


RE_MDH_URL_NO_QUERYSTR = re.compile(r"www.mdh.se/.*(\?kod=)[0-9a-zA-ZåäöÅÄÖ]+", re.UNICODE)
RE_MDH_URL = re.compile(r"www.mdh.se/.*", re.UNICODE)


def clean_mdh_url(url_to_clean):
    #     http://www.mdh.se/utbildning/kurser?kod=SAA055&l=en
    m = RE_MDH_URL.search(url_to_clean)
    if m:
        m_group = m.group(0)
        if '?kod=' in m_group:
            m2 = RE_MDH_URL_NO_QUERYSTR.search(m_group)
            m_group = m2.group(0)
            # print(m_group)
        return m_group
    else:
        raise ValueError('Could not clean url: %s' % url_to_clean)


from urllib.parse import urlparse


def remove_querystring(url):
    return urlparse(url).path


# clean_mdh_url('http://www.mdh.se/utbildning/kurser?kod=SAA055&l=en')
# clean_mdh_url('https://www.mdh.se/utbildning/kurser?kod=SAA055')
# print(clean_mdh_url('http://www.mdh.se/utbildning/program/AGM05?l=sv'))

def add_url_protocol(original_url):
    return 'https://' + original_url


def get_redirect_url_in_mdh_se(original_url):
    # www.mdh.se/utbildning/program/GSV03?l=sv
    complete_url = add_url_protocol(original_url)

    response = requests.head(complete_url)

    status_code = response.status_code
    if 301 == status_code:
        print('Redirect from %s to %s' % (complete_url, response.next.url))
        return response.next.url, status_code
    elif 200 == status_code:
        print('No redirect for %s - http 200' % (complete_url))
        return complete_url, status_code
    elif 404 == status_code:
        print('Page %s not found, http 404' % complete_url)
        return None, status_code
    else:
        raise ValueError('Unhandled status_code: %s for url: %s' % (status_code, complete_url))


def find_count_diffs():
    mdh_json_data = get_all_mdh_data()
    print('len(mdh_json_data): %s' % len(mdh_json_data))

    cached_redirects = load_cached_redirect_urls()

    unique_clean_mdh_urls = set()

    for mdh_item in mdh_json_data:
        mdh_url = jmespath.search("education.url", mdh_item)
        clean_susa_url = clean_mdh_url(mdh_url)
        unique_clean_mdh_urls.add(clean_susa_url)
    # mdh_unique_ids = [mdh_item for mdh_item in mdh_json_data]

    susanavet_json_data = get_all_susa_navet_data()

    susa_mdh_courses = []
    unclean_susa_urls = []
    unique_clean_susa_urls = set()
    for susa_item in susanavet_json_data:
        susa_id = jmespath.search("id", susa_item)
        if '.mdh.' in susa_id:
            susa_mdh_courses.append(susa_item)
            susa_urls = jmespath.search("events[*].url.url[*].content", susa_item)

            if susa_urls:
                susa_urls = susa_urls[0]
                susa_url = susa_urls[0]
                if susa_url:
                    try:
                        clean_susa_url = clean_mdh_url(susa_url)
                        # print('Clean url:' + clean_susa_url)

                        unique_clean_susa_urls.add(clean_susa_url)
                    except ValueError as e:
                        unclean_susa_urls.append({'id': susa_id, 'url': susa_url})
                        # print('id: %s, exception: %s' % (susa_id, e))
                else:
                    print('id %s does not have a url' % susa_id)
            # print(json.dumps(susa_urls))
            # print(json.dumps(susa_item))
            # break

    print('len(susa_mdh_courses): %s' % len(susa_mdh_courses))
    print('len(unique_clean_mdh_urls): %s' % len(unique_clean_mdh_urls))

    print('len(unique_clean_susa_urls): %s' % len(unique_clean_susa_urls))
    print('len(unclean_susa_urls): %s' % len(unclean_susa_urls))
    # print(unclean_susa_urls)

    mdh_urls_in_susanavet = [url for url in unique_clean_mdh_urls if url in unique_clean_susa_urls]
    print('len(mdh_urls_in_susanavet): %s' % len(mdh_urls_in_susanavet))
    susanavet_urls_missing_in_mdh = [url for url in unique_clean_susa_urls if url not in unique_clean_mdh_urls]

    # print('\n'.join(susanavet_urls_missing_in_mdh))
    print('len(susanavet_urls_missing_in_mdh): %s' % len(susanavet_urls_missing_in_mdh))

    http_200_urls_missing_in_mdh = []
    http_404_urls_in_susa_navet = []

    missing_url_redirects = {}
    for counter, missing_url in enumerate(susanavet_urls_missing_in_mdh):
        # if counter < 100:
        if True:
            # print('get_redirect_url_in_mdh_se for %s' % missing_url)

            if missing_url in cached_redirects:
                redirect_url = cached_redirects[missing_url]
                status_code = 301
                # print('Got redirect for %s from cache (for %s)' % (missing_url, redirect_url))
            else:
                redirect_url, status_code = get_redirect_url_in_mdh_se(missing_url)

            if redirect_url and status_code == 301:
                clean_redirect_url = clean_mdh_url(redirect_url)
                if '?' in clean_redirect_url:
                    clean_redirect_url = remove_querystring(clean_redirect_url)
                missing_url_redirects[missing_url] = clean_redirect_url
                cached_redirects[missing_url] = clean_redirect_url
            elif status_code == 200:
                # print('Url %s is http 200 but was missing in mdh.se-items')
                http_200_urls_missing_in_mdh.append(redirect_url)
            elif status_code == 404:
                http_404_urls_in_susa_navet.append(add_url_protocol(missing_url))

    print('missing_url_redirects\n' + json.dumps(missing_url_redirects))
    print('http_200_urls_missing_in_mdh: %s' % len(http_200_urls_missing_in_mdh))
    print('http_200_urls_missing_in_mdh\n' + json.dumps(http_200_urls_missing_in_mdh))
    print('http_404_urls_in_susa_navet: %s' % len(http_404_urls_in_susa_navet))
    print('http_404_urls_in_susa_navet\n' + json.dumps(http_404_urls_in_susa_navet))

    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_rows', None)
    if len(missing_url_redirects) > 0:
        concept_term_list_df = pd.DataFrame.from_dict(missing_url_redirects, orient='index')
        concept_term_list_df.index.name = 'susa_navet_url'
        concept_term_list_df.columns = ['redirect_url']
        concept_term_list_df.to_csv(get_redirect_urls_filepath(), sep=';', encoding='utf-8-sig')

    # susanavet_urls_missing_in_mdh = [url for url in unique_clean_susa_urls if url not in unique_clean_mdh_urls]
    for original_url, redirect_url in missing_url_redirects.items():
        unique_clean_susa_urls.remove(original_url)
        unique_clean_susa_urls.add(redirect_url)

    print('len(susanavet_urls_missing_in_mdh) before checking redirect_urls: %s' % len(susanavet_urls_missing_in_mdh))
    susanavet_urls_missing_in_mdh = [url for url in unique_clean_susa_urls if url not in unique_clean_mdh_urls]

    # print('\n'.join(susanavet_urls_missing_in_mdh))
    print('len(susanavet_urls_missing_in_mdh) after checking redirect_urls: %s' % len(susanavet_urls_missing_in_mdh))

    clean_http_200_urls_missing_in_mdh = [clean_mdh_url(url) for url in http_200_urls_missing_in_mdh]
    clean_http_404_urls_in_susa_navet = [clean_mdh_url(url) for url in http_404_urls_in_susa_navet]

    remaining_missing_urls = [url for url in susanavet_urls_missing_in_mdh if
                              url not in clean_http_200_urls_missing_in_mdh and url not in clean_http_404_urls_in_susa_navet]
    print('remaining_missing_urls: %s' % len(remaining_missing_urls))

    # print(json.dumps(http_200_urls_missing_in_mdh))
    # print(json.dumps(http_404_urls_in_susa_navet))
    print(json.dumps(remaining_missing_urls))


def load_cached_redirect_urls():
    df = pd.read_csv(get_redirect_urls_filepath(), usecols=['susa_navet_url', 'redirect_url'], sep=';')
    result = df.to_dict(orient='records')

    cached_result = {}
    for item in result:
        cached_result[item['susa_navet_url']] = item['redirect_url']

    return cached_result


def get_redirect_urls_filepath():
    return currentdir + 'mdh_redirect_urls_in_navet.csv'


# def check_description_diffs():
#     pass


mdh_filepath = currentdir + '../resources/mdh_output_complete_2021-10-13.json'
# susanavet_filepath = currentdir + '../resources/susa_navet_output_200_items.json'
susanavet_filepath = currentdir + '../resources/susa_navet_output_complete_2021-10-04.json'

# print_json_structure(mdh_filepath)
# print_json_structure(susanavet_filepath)


# print_original_items_by_code('ENA801', susanavet_filepath, mdh_filepath)
# print_original_items_by_code('SAA055', '22018', susanavet_filepath, mdh_filepath)

find_count_diffs()
# check_description_diffs()

# print(load_cached_redirect_urls())

# get_redirect_url_in_mdh_se('www.mdh.se/utbildning/program/GSV03?l=sv')
# get_redirect_url_in_mdh_se('www.mdh.se/utbildning/program/KALLEANKA?l=sv')
