import copy
import json
import os

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def load_json_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def remove_values_recursive(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            # yield from remove_values_recursive(value)
            remove_values_recursive(value)
        elif type(value) is list:
            for counter, item in enumerate(value):
                if type(item) is dict:
                    remove_values_recursive(item)
                    # print('Dict key: %s, calling recursive' % (key))

                else:
                    # print('Setting list item to %s' % (str(type(item))))
                    value[counter] = str(type(item))
        else:
            dictionary[key] = str(type(value))
            # print('Setting key: %s to %s' % (key, str(type(value))))


def remove_values_recursive_shallow(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            # print('Dict key: %s, calling recursive' % (key))
            remove_values_recursive_shallow(value)
        elif type(value) is list:
            item = copy.deepcopy(value[0])
            value = []
            value.append(item)
            dictionary[key] = value
            if type(item) is dict:
                # print('Dict key in list item 0: %s, calling recursive' % (key))
                remove_values_recursive_shallow(item)
            else:
                # print('Setting list item to %s' % (str(type(item))))
                value[0] = type(item).__name__
        else:
            # print('Setting key: %s to %s' % (key, str(type(value))))
            dictionary[key] = type(value).__name__


def merge(a, b, path=None):
    "merges b into a"
    # print('a: %s' % a)
    # print('b: %s' % b)


    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge(a[key], b[key], path + [str(key)])
            elif isinstance(a[key], list) and isinstance(b[key], list):
                # for item in b[key]:
                    # print('Merging: a[key]: %s with item: %s' % (a[key], item))
                # for index, val in enumerate(b[key]):
                #     # print(idx, val)
                #     if isinstance(a[key], dict) and isinstance(b[key][index], dict):
                #         merge(a[key], b[key][index])



                if isinstance(a[key][0], dict) and isinstance(b[key][0], dict):
                    merge(a[key][0], b[key][0], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            # else:
            #     raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            # print('Adding missing key: %s' % key)
            a[key] = b[key]
    return a


def print_json_structure(filepath):
    json_data = load_json_file(filepath)

    nr_of_items = len(json_data)
    # nr_of_items = 200

    # sampleindex = len(json_data)-1

    item_merged = {}

    for index in range(0, nr_of_items):
        itemcopy = copy.deepcopy(json_data[index])
        # remove_values_recursive(itemcopy)
        # pprint(itemcopy)
        remove_values_recursive_shallow(itemcopy)
        print(('*' * 40) + '\nitemcopy\n' + ('-' * 40))
        print(json.dumps(itemcopy))
        print(('-' * 40) + '\nitemmerged\n' + ('-' * 40))
        item_merged = merge(itemcopy, item_merged)
        print(json.dumps(item_merged))



    #
    # itemcopy2 = copy.deepcopy(json_data[sampleindex - 1])
    # # remove_values_recursive(itemcopy)
    # # pprint(itemcopy)
    # remove_values_recursive_shallow(itemcopy2)
    # print('itemcopy2' + ('*' * 40))
    #
    # print(str(itemcopy2).replace('\'', '"'))
    #
    #
    # print('merged_dict' + ('*' * 40))
    # merged_dict = merge(itemcopy, itemcopy2)
    print(('*' * 40) + '\nitem_merged\n' + ('*' * 40))
    print(json.dumps(item_merged))
    # print('str(itemcopy) == str(itemcopy2): %s' % (str(itemcopy) == str(itemcopy2)))
    # print('str(itemcopy) == str(merged_dict): %s' % (str(itemcopy) == str(merged_dict)))
    # print('str(itemcopy2) == str(merged_dict): %s' % (str(itemcopy2) == str(merged_dict)))


# mdh_filepath = currentdir + '../resources/mdh_output.json'
mdh_filepath = currentdir + '../resources/mdh_output_complete_2021-10-06.json'

# susanavet_filepath = currentdir + '../resources/susa_navet_output_2ish_items.json'
# susanavet_filepath = currentdir + '../resources/susa_navet_output_200_items.json'
susanavet_filepath = currentdir + '../resources/susa_navet_output_complete_2021-10-04.json'

# print_json_structure(mdh_filepath)
print_json_structure(susanavet_filepath)
