import json
import sys
import os
import PySimpleGUI as sg
from operator import itemgetter
from find_nodesc_edu_by_provider_in_susa import get_edu_tree

"""
input file:
A complete scraping with education-scraping/arbetsformedlingen/spiders/susanavet.py
with SKIP_ITEMS_FORMAT = False
default input file:
../resources/susa-all-15nov.json
"""

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def education_info(edu):
    #return (edu.get('education').get('title')[0].get('content'), edu.get('id'))
    return edu.get('id')


def count_nested_dict(d):
    if isinstance(d, dict):
        return sum(count_nested_dict(v) for v in d.values())
    elif isinstance(d, list):
        return len(d)
    else:
        return 1


def build_edu_tree(treedata, dic):
    for myndighet in dic:
        treedata.Insert('', myndighet, myndighet, values=[count_nested_dict(dic[myndighet])])
        for skoltyp in dic[myndighet]:
            treedata.Insert(myndighet, skoltyp, skoltyp, values=[count_nested_dict(dic[myndighet][skoltyp])])
            for skola in sorted(dic[myndighet][skoltyp], key=itemgetter(0)):
                treedata.Insert(skoltyp, skola, skola, values=[count_nested_dict(dic[myndighet][skoltyp][skola])])
                edu_list = [education_info(edu) for edu in dic[myndighet][skoltyp][skola]]
                for edu_tup in sorted(edu_list, key=lambda t: t[0].lower()):
                    treedata.Insert(skola, edu_tup[0], edu_tup[0], values=[edu_tup[1]])


def tree_view(dic):
    treedata = sg.TreeData()
    build_edu_tree(treedata, dic)
    layout = [[sg.Tree(data=treedata,
                       headings=['                    ', ],
                       auto_size_columns=True,
                       num_rows=25,
                       col0_width=120,
                       key='-TREE-',
                       show_expanded=False,
                       enable_events=True,
                       justification="right"),
               ],
              [sg.Button('Close')]]
    window = sg.Window('SUSA-navet browser', layout)
    while True:  # Event Loop
        event, values = window.read()
        if event in (sg.WIN_CLOSED, 'Close'):
            break
    window.close()
    del window


if len(sys.argv) >= 2:
    susanavet_filepath = sys.argv[1]
else:
    susanavet_filepath = currentdir + '../resources/susa-all-26nov.json'
with open(susanavet_filepath, 'r', encoding='utf-8') as file:
    edu_list = json.load(file)
edu_tree = get_edu_tree(edu_list)
tree_view(edu_tree)
