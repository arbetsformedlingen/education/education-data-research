import os

ENRICH_CALLS_PARALLELISM = int(os.getenv('ENRICH_CALLS_PARALLELISM', '8'))
# JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-api.jobtechdev.se')
JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-test-api.jobtechdev.se')
JAE_API_TIMEOUT_SECONDS = int(os.getenv('JAE_API_TIMEOUT_SECONDS', '60'))

JAE_API_KEY = os.getenv('JAE_API_KEY', 'api key here')

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 19200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv('ES_USE_SSL', 'false').lower() == 'true'
ES_VERIFY_CERTS = os.getenv('ES_VERIFY_CERTS', 'false').lower() == 'true'

VERSION = '0.0.1'

OCCUPATIONS_ALIAS_NAME = os.getenv('OCCUPATIONS_ALIAS_NAME', 'enriched-occupations')
JOBTITLES_ALIAS_NAME = os.getenv('JOBTITLES_ALIAS_NAME', 'enriched-jobtitles')

EDUCATIONS_SUSANAVET_ALIAS_NAME = os.getenv('EDUCATIONS_SUSANAVET_ALIAS_NAME', 'educations-susanavet')

ENRICHED_OCCUPATIONS_INDEX_CONFIG = {
    "settings": {
        "analysis": {
            "normalizer": {
                "occupationnormalizer": {
                    "type": "custom",
                    "char_filter": [],
                    "filter": ["lowercase"]
                }
            }
        }
    },
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            },
            "occupation": {
                "properties": {
                    "label": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "normalizer": "occupationnormalizer",
                                "ignore_above": 256
                            }
                        }
                    },
                    "occupation_group": {
                        "properties": {
                            "label": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "normalizer": "occupationnormalizer",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
