import logging

import jmespath
import settings
from opensearch_common import get_os_client

log = logging.getLogger(__name__)


def get_susa_education_by_id(susa_id):
    query = {
        "query": {
            "match": {
                "education.identifier": susa_id.lower()
            }
        }
    }

    os_client = get_os_client()
    results = os_client.search(index=settings.EDUCATIONS_SUSANAVET_ALIAS_NAME, body=query,
                               track_total_hits=True)

    # print(json.dumps(results, ensure_ascii=False, indent=4))

    hits = jmespath.search("hits.hits", results)
    if hits:
        hit = hits[0]
        return hit['_source']
    else:
        return None



# susa_ed = get_susa_education_by_id('i.uoh.mdh.bma075.22049.20212')
# print(json.dumps(susa_ed, ensure_ascii=False, indent=4))