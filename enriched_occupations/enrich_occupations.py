import os
import json
import sys
import time
from opensearchpy import NotFoundError

from multiprocessing import Value
import jmespath
import itertools
import settings
import copy
import concurrent.futures
import requests
from collections import Counter
from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException
from education_opensearch.opensearch_store import OpensearchStore
from education_opensearch.repository import opensearch_client
import logging

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

counter = Value('i', 0)

ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
ENRICHER_RETRIES = 10

FILEPATH_JOBAD_ID_2_LANGCODE = 'jobad_id_lang_code_2020_2021.json'

def load_json_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def get_enrich_result(batch_indata, jae_api_endpoint_url, timeout):
    print('Getting result from endpoint: %s' % jae_api_endpoint_url)

    headers = {'Content-Type': 'application/json', 'api-key': settings.JAE_API_KEY}

    for retry in range(ENRICHER_RETRIES):
        try:
            r = requests.post(url=jae_api_endpoint_url, headers=headers, json=batch_indata, timeout=timeout)
            r.raise_for_status()
        except Exception as e:
            print(f"get_enrich_result() retrying #{retry + 1} after error: {e}")
            time.sleep(0.5)
        else:
            return r.json()
    print(f"_get_enrich_result failed after: {ENRICHER_RETRIES} retries with error. Exit!")
    sys.exit(1)

def execute_calls(batch_indatas, jae_api_endpoint_url, parallelism):
    global counter
    enriched_output = {}
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the load operations and mark each future with its URL
        future_to_enrich_result = {executor.submit(get_enrich_result, batch_indata, jae_api_endpoint_url,
                                                   settings.JAE_API_TIMEOUT_SECONDS): batch_indata
                                   for batch_indata in batch_indatas}
        for future in concurrent.futures.as_completed(future_to_enrich_result):
            try:
                enriched_result = future.result()
                for resultrow in enriched_result:
                    enriched_output[resultrow[ENRICHER_PARAM_DOC_ID]] = resultrow
                    with counter.get_lock():
                        counter.value += 1
                        if counter.value % 1000 == 0:
                            print(f'enrichtextdocuments - Processed docs: {counter.value}')
            except Exception as e:
                print(e)
                raise

    return enriched_output

def enrich(ad_batches, batch_indata_config, endpoint_url, parallelism):
    batch_indatas = []
    for i, ad_batch in enumerate(ad_batches):
        ad_batch_indatas = [ad_indata for ad_indata in ad_batch]
        batch_indata = copy.deepcopy(batch_indata_config)
        batch_indata['documents_input'] = ad_batch_indatas
        batch_indatas.append(batch_indata)
    print('len(batch_indatas): %s' % len(batch_indatas))
    enrich_results_data = execute_calls(batch_indatas, endpoint_url, parallelism)
    # print_debug('enrich_results_data: %s' % enrich_results_data)

    return enrich_results_data

def get_empty_output_value():
    return {
        "enriched_candidates": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        }
    }


def get_null_safe_value(element, key, replacement_val):
    val = element.get(key, replacement_val)
    if val is None:
        val = replacement_val
    return val

def add_if_any_value(value, headline_parts):
    if value:
        headline_parts.append(value)

def get_doc_headline_input(jobad):
    sep = ' | '
    headline_parts = []

    # doc_headline_occupation = sep.join([occupation.get('label', '') for occupation in jobad.get('occupation', {})])
    add_if_any_value(jmespath.search("occupation.label", jobad), headline_parts)

    wp_address_node = jmespath.search("workplace_address", jobad)#jobad.get('workplace_address', {})

    if wp_address_node:
        add_if_any_value(get_null_safe_value(wp_address_node, 'city', ''), headline_parts)
        add_if_any_value(get_null_safe_value(wp_address_node, 'municipality', ''), headline_parts)
        add_if_any_value(get_null_safe_value(wp_address_node, 'region', ''), headline_parts)
        add_if_any_value(get_null_safe_value(wp_address_node, 'country', ''), headline_parts)

    add_if_any_value(get_null_safe_value(jobad, 'headline', ''), headline_parts)

    doc_headline_input = sep.join(headline_parts)

    return doc_headline_input

def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])

def add_enriched_result(items_to_update, enriched_results_data):

    for item_to_update in items_to_update:
        if not 'text_enrichments_results' in item_to_update:
            item_to_update['text_enrichments_results'] = {}
        doc_id = str(item_to_update.get('id', ''))
        if doc_id in enriched_results_data:
            enriched_output = enriched_results_data[doc_id]
            enriched_output.pop(ENRICHER_PARAM_DOC_ID, None)
            enriched_output.pop(ENRICHER_PARAM_DOC_HEADLINE, None)
        else:
            # Set non valid ads to empty enriched values.
            enriched_output = get_empty_output_value()

        item_to_update['text_enrichments_results'] = enriched_output

def detect_langcode(text_for_detection):
    if not text_for_detection:
        return "xx"

    # Note: If text to detect is in uppercase, the langdetect function might predict the language to be german instead of (correct) swedish.
    text_for_detection = text_for_detection.lower()
    try:
        lang_code = detect(text_for_detection)
        return lang_code
    except LangDetectException:
        # Can't detect lang_code
        # print('Error, text for detection: %s' % text)
        return "xy"

def format_concept_labels(enriched_candidates):
    return [candidate['concept_label'].lower() for candidate in enriched_candidates]

def count_terms(concept_terms):
    counter_most_common = Counter(concept_terms).most_common()
    term_count_list = []
    for counttuple in counter_most_common:
        term_count_list.append({"concept": counttuple[0], "occurences": counttuple[1]})

    return term_count_list



def process_ads(start_ad_index=None, stop_ad_index=None):
    # jobads = load_json_file('jobstream_ads_stream.json')
    # jobads = load_json_file('jobstream_ads_snapshot.json')

    jobads = load_json_file('historical_ads_2020.json')
    jobads.extend(load_json_file('historical_ads_2021_first_6_months.json'))

    if start_ad_index and stop_ad_index:
        jobads = jobads[start_ad_index:min(stop_ad_index, start_ad_index + len(jobads))]
    elif start_ad_index:
        jobads = jobads[start_ad_index:]
    elif stop_ad_index:
        jobads = jobads[:stop_ad_index]

    total_jobads_size = len(jobads)
    print('Loaded %s jobads for processing.' % total_jobads_size)
    # jobads = jobads[0:100]

    parallelism = settings.ENRICH_CALLS_PARALLELISM

    if parallelism <= 0:
        parallelism = 1

    ad_batches = prepare_enrich_input(jobads)
    batch_indata_config = {
        "include_terms_info": True,
        "include_sentences": False,
        "include_synonyms": False,
        "include_misspelled_synonyms": False
    }

    enrich_results = enrich(ad_batches, batch_indata_config, settings.JAE_API_URL + '/enrichtextdocumentsbinary', parallelism)
    add_enriched_result(jobads, enrich_results)

    # if index_name:
    enriched_aggrated_occupations = load_enriched_aggrated_occupations_from_repository(settings.OCCUPATIONS_ALIAS_NAME)
    print('Loaded %s previously saved items from alias %s' % (len(enriched_aggrated_occupations), settings.OCCUPATIONS_ALIAS_NAME))
        # enriched_aggrated_occupations = {}
    # else:
    #     enriched_aggrated_occupations = {}
    # pprint(jobads[0])

    extend_enriched_data_for_occupation(enriched_aggrated_occupations, jobads)



    # print(json.dumps(enriched_aggrated_occupations))
    unique_competencies = set()
    unique_geos = set()
    unique_occupations = set()
    unique_traits = set()

    for occupation, value in enriched_aggrated_occupations.items():
        candidates_node = enriched_aggrated_occupations[occupation]['occupation']['enriched_candidates']

        candidates_node['competencies'] = sorted(jmespath.search('occupation.enriched_candidates.competencies', value))
        candidates_node['geos'] = sorted(jmespath.search('occupation.enriched_candidates.geos', value))
        candidates_node['occupations'] = sorted(jmespath.search('occupation.enriched_candidates.occupations', value))
        candidates_node['traits'] = sorted(jmespath.search('occupation.enriched_candidates.traits', value))

        for concept in candidates_node['competencies']:
            unique_competencies.add(concept)

        for concept in candidates_node['geos']:
            unique_geos.add(concept)

        for concept in candidates_node['occupations']:
            unique_occupations.add(concept)

        for concept in candidates_node['traits']:
            unique_traits.add(concept)


    unique_concepts = {
        "unique_competencies": sorted(list(unique_competencies)),
        "unique_geos": sorted(list(unique_geos)),
        "unique_occupations": sorted(list(unique_occupations)),
        "unique_traits": sorted(list(unique_traits))
    }

    with open('enriched_occupations_unique_concepts.json', 'w', encoding='utf-8') as f:
        json.dump(unique_concepts, f, ensure_ascii=False, indent=4)

    enriched_occupations_to_store = list(enriched_aggrated_occupations.values())
    # print(enriched_aggrated_occupations)
    with open('enriched_occupations_snapshot4.json', 'w', encoding='utf-8') as f:
        json.dump(enriched_occupations_to_store, f, ensure_ascii=False, indent=4)

    save_enriched_occupations_in_repository(enriched_occupations_to_store)


def extend_enriched_data_for_occupation(enriched_aggrated_occupations, enriched_jobads):
    for enriched_jobad in enriched_jobads:
        occupation = jmespath.search("occupation.label", enriched_jobad)

        enriched_candidates = jmespath.search('text_enrichments_results.enriched_candidates', enriched_jobad)
        enriched_competencies = format_concept_labels(jmespath.search('competencies', enriched_candidates))
        enriched_geos = format_concept_labels(jmespath.search('geos', enriched_candidates))
        enriched_occupations = format_concept_labels(jmespath.search('occupations', enriched_candidates))
        enriched_traits = format_concept_labels(jmespath.search('traits', enriched_candidates))

        if not occupation in enriched_aggrated_occupations:
            enriched_aggrated_occupations[occupation] = {
                "id": jmespath.search("occupation.concept_id", enriched_jobad),
                "occupation": {
                    "enriched_ads_count": 0,
                    "concept_id": jmespath.search("occupation.concept_id", enriched_jobad),
                    "label": occupation,
                    "legacy_ams_taxonomy_id": jmespath.search("occupation.legacy_ams_taxonomy_id", enriched_jobad),
                    "occupation_group": {
                        "concept_id": jmespath.search("occupation_group.concept_id", enriched_jobad),
                        "label": jmespath.search("occupation_group.label", enriched_jobad),
                        "legacy_ams_taxonomy_id": jmespath.search("occupation_group.legacy_ams_taxonomy_id", enriched_jobad)
                    },
                    'enriched_candidates': {'competencies': [],
                                            'geos': [],
                                            'occupations': [],
                                            'traits': []
                                            }

                }
            }

        enriched_occupation_node = enriched_aggrated_occupations[occupation]['occupation']

        enriched_occupation_node["enriched_ads_count"] = enriched_occupation_node["enriched_ads_count"] + 1
        candidates_node = enriched_occupation_node['enriched_candidates']
        candidates_node['competencies'].extend(enriched_competencies)
        candidates_node['geos'].extend(enriched_geos)
        candidates_node['occupations'].extend(enriched_occupations)
        candidates_node['traits'].extend(enriched_traits)


def prepare_enrich_input(jobads):
    total_jobads_size = len(jobads)
    cached_langcodes = load_cached_langcodes()
    nr_of_items_per_batch = 100
    # Prepare input
    ads_input_data = []
    input_ad_counter = 0
    added_ad_ids = set()
    duplicate_ids = []
    removed_ids = []
    jobad_id_lang_code = {}

    for jobad in jobads:
        doc_id = str(jobad.get('id', ''))

        is_removed = jobad.get('removed', False)

        if is_removed:
            # print('jobad id %s is removed!' % doc_id)
            removed_ids.append(doc_id)
            continue

        if doc_id in added_ad_ids:
            # Skip duplicate ids.
            duplicate_ids.append(doc_id)
            continue
        else:
            added_ad_ids.add(doc_id)

        doc_headline = get_doc_headline_input(jobad)
        doc_text = jobad.get('description', {}).get('text_formatted', '')

        if not doc_text:
            doc_text = jobad.get('description', {}).get('text', '')

        if doc_id in cached_langcodes:
            detected_lang_code = cached_langcodes[doc_id]
        else:
            # Check language, only enrich for swedish ads.
            text_for_detection = None
            if doc_text:
                text_for_detection = doc_text.strip()[:400]

            detected_lang_code = detect_langcode(text_for_detection)

        jobad_id_lang_code[doc_id] = detected_lang_code
        # print('Detected langcode: %s' % detected_lang_code)

        if 'sv' == detected_lang_code:
            input_doc_params = {
                ENRICHER_PARAM_DOC_ID: doc_id,
                ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
                ENRICHER_PARAM_DOC_TEXT: doc_text
            }

            ads_input_data.append(input_doc_params)

        input_ad_counter += 1
        if input_ad_counter % 1000 == 0:
            print(f'Prepared input for {input_ad_counter} ads')

    with open(FILEPATH_JOBAD_ID_2_LANGCODE, 'w', encoding='utf-8') as f:
        json.dump(jobad_id_lang_code, f, ensure_ascii=False, indent=4)
    print(f'Found {len(duplicate_ids)} duplicate ids in {total_jobads_size} jobads')
    print(f'Found {len(removed_ids)} removed ids in {total_jobads_size} jobads')

    print('Prepared to enrich %s/%s jobads' % (len(ads_input_data), total_jobads_size))
    ad_batches = grouper(nr_of_items_per_batch, ads_input_data)

    return ad_batches


def save_enriched_occupations_in_repository(enriched_occupations_to_store):
    os_store = OpensearchStore()

    index_name = os_store.start_new_save(settings.OCCUPATIONS_ALIAS_NAME, mappings=settings.ENRICHED_OCCUPATIONS_INDEX_CONFIG)

    print('Will save enriched occupations in index_name: %s' % index_name)

    os_store.save_educations_to_repository(enriched_occupations_to_store)
    # OCCUPATIONS_ALIAS_NAME = 'enriched-occupations'
    if not opensearch_client.alias_exists(settings.OCCUPATIONS_ALIAS_NAME):
        opensearch_client.put_alias(index_name, settings.OCCUPATIONS_ALIAS_NAME)
    else:
        current_alias = opensearch_client.get_alias(settings.OCCUPATIONS_ALIAS_NAME)
        opensearch_client.update_alias(index_name, [key for key in current_alias], settings.OCCUPATIONS_ALIAS_NAME)


def load_cached_langcodes():
    cached_lang_codes = load_json_file(FILEPATH_JOBAD_ID_2_LANGCODE)
    print('Loaded %s cached langcodes' % len(cached_lang_codes))

    return cached_lang_codes

def load_enriched_aggrated_occupations_from_repository(index_name):
    query = {
        "track_total_hits": True,
        "query": {"match_all": {}}
    }

    print("Using index: %s" % index_name)

    results = opensearch_client.scan_search(
        query,
        index_name
    )

    enriched_aggrated_occupations = {}
    try:

        results = [result for result in results]

        for item in results:
            occupation_obj = item['_source']
            occupation_label = jmespath.search("occupation.label", occupation_obj)
            enriched_aggrated_occupations[occupation_label] = occupation_obj
    except NotFoundError:
        print('Existing index/alias %s not found in Opensearch' % index_name)

    return enriched_aggrated_occupations


# load_cached_langcodes()

# process_ads(start_ad_index=0, stop_ad_index=500, index_name='testing')

# process_ads(start_ad_index=0, stop_ad_index=500)

# INFO: Latest run - begin
# process_ads(start_ad_index=0, stop_ad_index=300000)
# process_ads(start_ad_index=300000, stop_ad_index=600000)
# process_ads(start_ad_index=600000, stop_ad_index=900000)
# INFO: Latest run - end

process_ads()